# A Survey on GANs for Anomaly Detection

#GAN #Anomaly

[pdf链接](https://arxiv.org/pdf/1906.11632.pdf)

[源码链接](github.com/zurutech/anomaly-toolbox)

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008135733.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008135905.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008135937.png)

## 主要创新点

## 步骤和过程

## 结果分析

综述，对比了几种异常数据生成的方法

## 启示

提供工具包，直接使用 #TODO