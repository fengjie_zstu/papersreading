## GAN相关论文

主要目的在于生成虚拟数据，构建新数据集，例如：通过标准车牌图片生成实际车牌图片，根据少量工业缺陷图片生成更多缺陷图片

重点关注：

- StyleGAN

---

## TO ADD

### Analyzing and Improving the Image Quality of {StyleGAN}

https://github.com/tg-bomze/StyleGAN2-Face-Modificator

### Encoding in Style: a StyleGAN Encoder for Image-to-Image Translation

https://github.com/eladrich/pixel2style2pixel

Official Implementation for "Encoding in Style: a StyleGAN Encoder for Image-to-Image Translation" (CVPR 2021) presenting the pixel2style2pixel (pSp) framework

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210809151744.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210809151705.png)

### StyleGAN2 Distillation for Feed-forward Image Manipulation

https://github.com/Gie-ok-Hie-ut/pytorch-stylegan2-distillation

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210809152328.png)

### StyleFlow: Attribute-conditioned Exploration of StyleGAN-Generated Images using Conditional Continuous Normalizing Flows

https://github.com/RameenAbdal/StyleFlow

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210809152528.png)

### Visualization of GAN training process

https://github.com/EvgenyKashin/gan-vis

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210809153041.png)

---

缺陷生成相关

### A Surface Defect Detection Method Based on Positive Samples

https://link.springer.com/chapter/10.1007/978-3-319-97310-4_54

阅读笔记：https://blog.csdn.net/qq_36104364/article/details/109255710 
https://blog.csdn.net/qq_41742361/article/details/108091996

https://github.com/mingren8888/GAN-defect

Defect-GAN: High-Fidelity Defect Synthesis for Automated Defect Inspection

https://openaccess.thecvf.com/content/WACV2021/papers/Zhang_Defect-GAN_High-Fidelity_Defect_Synthesis_for_Automated_Defect_Inspection_WACV_2021_paper.pdf

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210810103941.png)