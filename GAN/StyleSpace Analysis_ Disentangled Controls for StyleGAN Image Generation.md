#StyleGAN

https://arxiv.org/pdf/2011.12799.pdf

代码：

[StyleSpace Analysis: Disentangled Controls for StyleGAN Image Generation](https://github.com/xrenaa/StyleSpace-pytorch)

预训练模型：

https://www.dropbox.com/s/c3aaq7i6soxmpzu/pretrained_stylegan2_ffhq.tar

https://github.com/betterze/StyleSpace

![car](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210809144822.png)

![bedroom](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210809144848.png)

![real face manipulation](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210809144907.png)