# Squeeze-and-Excitation Networks 笔记 (CVPR)
#2
[论文](https://arxiv.org/abs/1709.01507)
[代码](https://github.com/hujie-frank/SENet)

### 一、摘要
首先作者介绍了卷积网络：

1. 卷积核作为卷积神经网络的核心，通常被看做是在局部感受野上，将空间上（spatial）的信息和特征维度上（channel-wise）的信息进行聚合的信息聚合体。
2. 卷积神经网络由一系列卷积层、非线性层和下采样层构成，这样它们能够从全局感受野上去捕获图像的特征来进行图像的描述。

其次作者讲述了他人都在研究在空间上如何提升模型性能，而自己提出的模型创新的地方就在于考虑从channel-wise角度提升网络性能。作者描述他提出的SE模块通过学习的方式来自动获取到每个特征通道的重要程度，然后依照这个重要程度去提升有用的特征并抑制对当前任务用处不大的特征。
（以上参考：https://zhuanlan.zhihu.com/p/76033612）

并且在摘要中，作者指出自己在ILSVRC2017图像分类比赛中获得第一。

了解该论文可以参考作者自己写的文章（https://www.cnblogs.com/bonelee/p/9030092.html）
### 二、主要创新点
作者的创新便是提出SE模块，从channel-wise角度提升网络性能。

总体结构如下图：

![图片一](https://images.gitee.com/uploads/images/2021/0929/105909_28c42d9c_7615007.png "屏幕截图.png")

Ftr表示卷积等一般变换   Fsq表示Squeeze操作，Fex表示Excitation操作。 Fscale就是矩阵相乘的操作。Fsq+Fex+Fscale就是SE模块。

#### 1、Squeeze
就是一个global average pooling，公式如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/111531_26b8d052_7615007.png "屏幕截图.png")

Squeeze将每个二维的特征通道（H*W通道）变成一个实数，使用最终的输出是1*1*C，这个实数某种程度上具有全局的感受野，他表征着在特征通道上响应的全局分布。
#### 2、Excitation
其实就是两层线性层，不过在W1,W2的维度上略有设计。公式如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/113750_ec15f73d_7615007.png "屏幕截图.png")

g是relu， σ是sigmoid，W1是[C/R,C]，W2是[C,C/R]。对于参数R的选择在文中也有详细的描述。在RESNET50的基础上分析超参数R对于网络性能和计算量的影响，见下表，得到如果基本模型为resnet50时，R=16时，时最优的选择：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/114932_21695a8e_7615007.png "屏幕截图.png")

#### 3、如何运用
SE模块就是一个即插即用的模块，可以融入到任何想要加入的模型当中，在论中举了一个加入Inception的例子，方便理解，结构变化如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/115647_dc9384ea_7615007.png "屏幕截图.png")

分支出来的那一块就是SE模块，最后还需要scale，就是矩阵相乘 ，1*1*C与H*W*C相乘维度重新回到H*W*C。所以scale就是为了让维度转换回去，方便SE模块的使用。
### 三、结果

作者将SE模块嵌入到了很多网络进行试验，结果都不错。实验的数据有很多，自行阅读。
### 四、启示

感觉很多优秀的论文并非是创造了一个全新的模型，而是有效的组合已有的模块，赋予其意义，达到优秀的结果。

