https://arxiv.org/abs/2102.10772

We propose UniT, a Unified Transformer model to simultaneously learn the most prominent tasks across different domains, ranging from object detection to language understanding and multimodal reasoning. Based on the transformer encoder-decoder architecture, our UniT model encodes each input modality with an encoder and makes predictions on each task with a shared decoder over the encoded input representations, followed by task-specific output heads. The entire model is jointly trained end-to-end with losses from each task. Compared to previous efforts on multi-task learning with transformers, we share the same model parameters to all tasks instead of separately fine-tuning task-specific models and handle a much higher variety of tasks across different domains. In our experiments, we learn 7 tasks jointly over 8 datasets, achieving comparable performance to well-established prior work on each domain under the same supervision with a compact set of model parameters.

### 代码

https://mmf.sh/（coming soon 2021.2.24）

### 框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210224084353.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210224084521.png)

### 结果

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210224084819.png)

### 启示

融合多个任务，每个任务有不同的角度，比单一任务会获得更好的结果吗？
可以确信的一点是模型会更加通用