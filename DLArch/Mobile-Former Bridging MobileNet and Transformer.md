#transformer 
#efficient

https://arxiv.org/abs/2108.05895

https://zhuanlan.zhihu.com/p/399569191

非官方代码：
https://github.com/ACheun9/Pytorch-implementation-of-Mobile-Former

https://github.com/slwang9353/MobileFormer

### 缘起
mobilenet局部 transformer全局

### 创新点
随机初始化少量标记，轻量级交叉注意力模型

### 结构
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916164456.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916164617.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916164715.png)

### 结果
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916164757.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916164909.png)