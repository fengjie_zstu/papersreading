#HRNet

https://arxiv.org/abs/1908.07919

### 代码

https://github.com/HRNet

### 解释

视频讲解 https://www.bilibili.com/video/BV12J411R7cH

作者团队的讲解

### 启示

如何从各个层之间获取特征，各个层之间的信息如何融合，HRNet给出了一个好的方案

减少通道数目，计算量并没有增加很多

刷了很多任务，分类，检测，分割，关键点。。。