# Multiscale Vision Transformers: A hierarchical architecture for representing image and video information

#Transformer

https://arxiv.org/pdf/2104.11227.pdf

https://ai.facebook.com/blog/multiscale-vision-transformers-an-architecture-for-modeling-visual-data/

https://github.com/facebookresearch/SlowFast

## 摘要与网络结构框图


![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211009100406.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211009100502.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211009100645.png)

## 主要创新点

在Transformer主干中开发一个时空特征层次。

在所有层中使用一个恒定的分辨率和特征维度，并使用一个注意力机制来决定它应该关注哪些先前的标记物。

MViT模型表现出对时间线索的卓越理解，而不会被虚假的空间偏差所束缚

## 步骤和过程

数据集：
Kinetics-400 [59] (K400) (∼240k training videos in 400 classes) and Kinetics-600 [11]. SomethingSomething-v2 [38], Charades [86], and AVA [39].

## 结果分析

## 启示

基于视频的描述或分析应尽快实践，通过对前一帧的分析定位信息加速当前帧的分析