#transformer

2021.9

http://arxiv-export-lb.library.cornell.edu/pdf/2109.00642
https://github.com/yilunliao/vit-search

### 缘起
如何利用CNN的设计技术提升transformer的性能

### 创新点
ViT-ResNAS：一个用神经结构搜索（NAS）设计的高效多阶段ViT架构

### 结构
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916151349.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916151458.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916151521.png)

### 结果
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916151800.png)