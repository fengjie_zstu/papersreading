# Deep Residual Learning for Image Recognition

[pdf链接]

https://openaccess.thecvf.com/content_cvpr_2016/html/He_Deep_Residual_Learning_CVPR_2016_paper.html

[源码链接]
https://github.com/KaimingHe/deep-residual-networks

## 摘要与网络结构框图

更深的神经网络更难训练。我们提出了一个残差学习框架，以简化比以前使用的网络更深的网络的训练。我们明确地将层重新表述为参考层输入学习残差函数，而不是学习未引用的函数。我们提供了全面的经验证据，表明这些残差网络更容易优化，并且可以从显着增加的深度中获得准确性。在 ImageNet 数据集上，我们评估了深度高达 152 层的残差网络——比 VGG 网络 [40] 深 8 倍，但仍然具有较低的复杂性。这些残差网络的集合在 ImageNet 测试集上实现了 3.57% 的错误率。该结果在 ILSVRC 2015 分类任务中获得第一名。我们还对具有 100 层和 1000 层的 CIFAR-10 进行了分析。
表示的深度对于许多视觉识别任务至关重要。仅仅由于我们极深的表示，我们在 COCO 对象检测数据集上获得了 28% 的相对改进。深度残差网络是我们向 ILSVRC & COCO 2015 竞赛提交的基础，我们还在 ImageNet 检测、ImageNet 定位、COCO 检测和 COCO 分割任务上获得了第一名。
![Figure 3](https://images.gitee.com/uploads/images/2021/1007/154602_36670645_8901265.png "截屏2021-10-07 下午3.45.35.png")
![Table 1](https://images.gitee.com/uploads/images/2021/1007/163902_b6d7d27d_8901265.png "截屏2021-10-07 下午4.20.36.png")
## 主要创新点

1) 深度残差网络非常容易优化，而原始网络在深度增加时训练误差会增大。
2) 深度残差网络在网络深度增加的同时能够轻易地提高准确度，比之前的网络都要好。

## 步骤和过程

文章首先提出了一个现象：当网络开始收敛时候，随着网络深度的增加，准确率开始变得饱和然后急剧下降。

这并不是由于过拟合，而是因为添加了过多的层所导致的高训练误差。

Figure 1是一个经典的例子。

> 这个问题叫做退化问题（degradation problem）。
![Figure 1](https://images.gitee.com/uploads/images/2021/1007/154133_5738ef7f_8901265.png "截屏2021-10-07 下午2.46.44.png")


然后提出深度残差学习框架解决这个退化问题。

设原来想要拟合的函数为$H(x)$，那么残差函数就为$F(x):=H(x)-x$。

所以$H(x) = F(x)+x$。

尽管两种形式应该都能近似到想要的函数，但是学习的容易程度可能不一样。


**通过捷径连接（Shortcut Connections）来实现$H(x) = F(x) + x$。**

对于Figure 2的两层block来说：

$y = F + x$，$y = F + W_sx$， $ F = W_2\sigma(W_1x) $，$\sigma$是ReLU函数。

这里$+x$执行的是恒等变换捷径（Identity Mapping Shortcuts），而$+W_sx$执行的是投影变换捷径（Projection Mapping Shortcuts），前者无额外参数，后者多一个参数$W_s$。

其次，由于$F+x$是矩阵加法，可能会有维度/通道不同，所以在维度/通道不同的时候，有两种选择：

选择A：

填充0以保持相同的维度/通道。（Identity Mapping）

选择B：

对$x$进行维度/通道变换：$W_sx$ （Projection Mapping）

> 一个最小的残差块是两层，如果只有一层，残差块就和线性层类似，就没有优势了。
![Figure 2](https://images.gitee.com/uploads/images/2021/1007/154323_63b9d901_8901265.png "截屏2021-10-07 下午3.42.49.png")


**34层残差网络**


每个虚线捷径都是通道的变换，这时候$F + x$两个通道不同，选择A或B来增加$x$的通道数。
![Figure 3](https://images.gitee.com/uploads/images/2021/1007/154602_36670645_8901265.png "截屏2021-10-07 下午3.45.35.png")

## 结果分析

#### ImageNet

##### 18层原始网络和34层原始网络的对比

Table 2:
![Table 2](https://images.gitee.com/uploads/images/2021/1007/154759_1f5bc187_8901265.png "截屏2021-10-07 下午3.47.26.png")

Figure 4：
![Figure 4](https://images.gitee.com/uploads/images/2021/1007/154817_3df4afd9_8901265.png "截屏2021-10-07 下午3.47.06.png")


34层原始网络在训练集和验证集上错误率都比18层要高。

##### 18层残差网络（选项A）和34层残差网络（选项A）的对比

34层残差网络错误率比18层的低。表明了**退化问题**在这个网络中可以解决。

18层残差网络和18层原始网络错误率几乎没有区别，但前者收敛更快。

##### 恒等捷径（Identity Shortcuts）和投影捷径（Projection Shortcuts）

1. 所有捷径都是不额外添加参数，需要添加维度/通道时采用选择A

2. 添加维度/通道时采用选择B，其他都是恒等捷径

3. 所有捷径都采用选择B

三种之间的差距不是太大，第三种虽然错误率很低，但是会增加模型大小。

##### 两层block和三层block

虽然三层有更多的层数，但两者的时间复杂度却相似。
![Table 3](https://images.gitee.com/uploads/images/2021/1007/155007_e60a011c_8901265.png "截屏2021-10-07 下午3.49.45.png")

在34层残差网络中，用三层block替换原来的两层block，形成50层残差网络。

50层残差网络错误率更低了，**但复杂度仅仅增加了0.2GFLOPs**。

用三层block组成101层和152层残差网络，与VGG-16/19网络相比，错误率低并且复杂度低。
![Figure 5](https://images.gitee.com/uploads/images/2021/1007/163709_fd41460c_8901265.png "截屏2021-10-07 下午4.13.24.png")
Table3和4
![Table 4](https://images.gitee.com/uploads/images/2021/1007/155144_958ceb5b_8901265.png "截屏2021-10-07 下午3.51.18.png")

##### 与当前最好的模型比较

152层残差网络的结果优于之前的网络的集成结果。

仅用两个152层残差网络赢得了ILSVRC 2015第一名。

Table5
![Table 5](https://images.gitee.com/uploads/images/2021/1007/155233_a41315c1_8901265.png "截屏2021-10-07 下午3.52.20.png")


#### CIFAR-10

> 旨在研究深度很大的网络的行为
>
> 使用Figure 3中的原始架构和残差架构，第一层是3*3卷积层，然后有三个部分，每个部分有2*n个卷积层，每个部分对应的特征图大小分别是{32,16,8}，每个部分对应的过滤器的数量分别是{16,32,64}（通道数量）

#### 层反馈的分析

通常，残差网络比原始网络有更小的反馈。

![Figure 7](https://images.gitee.com/uploads/images/2021/1007/155534_7de2229d_8901265.png "截屏2021-10-07 下午3.54.28.png")
这些结果也说明了残差函数比非残差函数更接近于0，更深的残差网络有更小的反馈幅度

##### 超过1000层的残差网络

层数为1202的残差网络，训练误差和测试误差仍然保持很低。

测试误差比110层的要高，可能是因为过拟合（因为没有Dropout）。

Figure 6 和 Table 6
![Figure 6](https://images.gitee.com/uploads/images/2021/1007/155600_5c1c08c2_8901265.png "截屏2021-10-07 下午3.54.05.png")
![Table 6](https://images.gitee.com/uploads/images/2021/1007/155723_ead59e50_8901265.png "截屏2021-10-07 下午3.57.10.png")

#### 在PASCAL和MS COCO上的物体检测

残差网络有好的泛化性能

Table 7和Table 8
![Table 7和Table 8](https://images.gitee.com/uploads/images/2021/1007/155737_3414a4f6_8901265.png "截屏2021-10-07 下午3.55.11.png")

用残差网络替换VGG-16，在COCO数据集上结果更优。



## 启示

