# Action Recognition Using Visual Attention (2016)

#3 [pdf](https://arxiv.org/pdf/1511.04119.pdf) [代码](https://github.com/MRzzm/action-recognition-models-pytorch.git)
### 一、摘要
作者提出基于soft attention的模型，整体的架构用的是LSTM模型。模型可以高效的学习视频中的那些部分跟task匹配。并且作者在UCF-11、HMDB-51和Hollywood2数据集上验证，并且分析场景和动作对模型将注意力集中在何处的影响。

 **注：** 
关于soft attention和hard attention可见https://zhuanlan.zhihu.com/p/52958865。在这里面还有解释bottom-up和top-down。
### 二、主要创新点
提出了一种soft attention方法嵌入到LSTM网络结构中

总的结构图如下：

![总结构图](https://images.gitee.com/uploads/images/2021/0930/114724_ea1bc8f6_7615007.png "屏幕截图.png")

左图是右图Lt与Xt结合的过程。Xt是第t帧视频经过CNN网络（论文中使用的是GooleNet）得到的feature map。Lt是attention map也就是论文的soft attention。Lt与Xt结合之后便输入LSTM网络。

#### 1. attention map（Lt）的获得
得到Lt的公式如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/144438_356d0ef6_7615007.png "屏幕截图.png")

首先ht-1就是LSTM t-1时刻的输出，将LSTM的输出经过线性层，并作softmax的处理得到Lt。Lt,i中的i表示几个patch。网络会将ht-1分为k*k个patch计算分数。分成几个patch根据 具体的情况定。

#### 2. Lt与Xt结合
公式如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/145151_b74c3b80_7615007.png "屏幕截图.png")

Xt指的是每一个patch，总共有K*K个，对应位置的score（Lt,i）和patch相乘然后求和。

#### 3. LSTM网络
LSTM的结构如总结构图右图所示，总体结构非常清晰，需要注意的是第一个时刻的cell state C0和hidden state H0并不是0，而是有初始化的。初始化公式如下，目的是为了加快收敛：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/145631_bdf069d0_7615007.png "屏幕截图.png")

很显然，初始的状态就是x每个时刻每个patch的取均。

除了对c0和h0的初始化，作者还修改了loss函数，在交叉熵函数上加上了正则项等。（对loss函数不是很明白）公式如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/150134_e7f956d6_7615007.png "屏幕截图.png")

### 三、结果
论文中用平均池化层和最大池化层来代替attention map，结果都不如attention，并且探究了损失函数中的入对实验室的影响，结果如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/150415_40a84fd7_7615007.png "屏幕截图.png")

在论文中，作者还分析了attention关注的区域，并且可视化出来，还可视化了一些失败的attention的尝试。不过它与其他SOTA模型相比，指标还是相差很大：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/151530_c2db89ea_7615007.png "屏幕截图.png")

### 四、启发

写论文时，也可以尝试可视化一些东西，让论文更加直观。