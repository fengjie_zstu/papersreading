#MLP

2021.9

https://arxiv.org/abs/2109.04454

https://github.com/SHI-Labs/Convolutional-MLPs

### 缘起
MLP被发现达到transformer结构相近的效果，但缺点在于1：输入固定，2:计算量大

### 创新点
轻量级的、分阶段的、共同设计的卷积层和MLPs

### 结构
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916144726.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916144759.png)

### 对比
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916145109.png)

### 效果
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916144916.png)