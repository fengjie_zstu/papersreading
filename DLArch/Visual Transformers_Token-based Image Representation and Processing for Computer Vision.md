https://arxiv.org/abs/2006.03677

Computer vision has achieved remarkable success by (a) representing images as uniformly-arranged pixel arrays and (b) convolving highly-localized features. However, convolutions treat all image pixels equally regardless of importance; explicitly model all concepts across all images, regardless of content; and struggle to relate spatially-distant concepts. In this work, we challenge this paradigm by (a) representing images as semantic visual tokens and (b) running transformers to densely model token relationships. Critically, our Visual Transformer operates in a semantic token space, judiciously attending to different image parts based on context. This is in sharp contrast to pixel-space transformers that require orders-of-magnitude more compute. Using an advanced training recipe, our VTs significantly outperform their convolutional counterparts, raising ResNet accuracy on ImageNet top-1 by 4.6 to 7 points while using fewer FLOPs and parameters. For semantic segmentation on LIP and COCO-stuff, VT-based feature pyramid networks (FPN) achieve 0.35 points higher mIoU while reducing the FPN module's FLOPs by 6.5x.

### 代码

https://github.com/tahmid0007/VisualTransformers

### 框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223084415.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223084610.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223084644.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223084740.png)

### 结果

性能方面与ResNet对比
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223084848.png)

准确度
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223084938.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223085012.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223085250.png)

tokens数目关系不大
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223085138.png)

分割结果

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223085410.png)

可视化

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223085611.png)

### 启示

vt效果更好，计算量还有所下降？【有代码cifar10，可以实验】