#transformer 
#efficient

2021.8 

https://arxiv.org/pdf/2108.13015.pdf

### 缘起
基于transforms的图像识别模型DeiT等随着复杂度的下降准确率下降很多，如何解决？

### 发现
vision transformer更适合处理高层次的信息，而不是低层次的特征

### 创新点
引入两个结构IPE，APM，在不同FLOP的情况下对比ImageNet的准确度

不同FLOP的模型设计

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916143733.png)

### 网络结构
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916143530.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916143558.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916143619.png)


### 结果
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916143504.png)