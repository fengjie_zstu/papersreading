github.com/vtddggg/Robust-Vision-Transformer 

https://www.arxiv-vanity.com/papers/2105.07926/

Recent advances on Vision Transformers (ViT) have shown that self-attention-based networks, which take advantage of long-range dependencies modeling ability, surpassed traditional convolution neural networks (CNNs) in most vision tasks. To further expand the applicability for computer vision, many improved variants are proposed to re-design the Transformer architecture by considering the superiority of CNNs, i.e., locality, translation invariance, for better performance. However, these methods only consider the standard accuracy or computation cost of the model. In this paper, we rethink the design principles of ViTs based on the robustness. We found some design components greatly harm the robustness and generalization ability of ViTs while some others are beneficial. By combining the robust design components, we propose Robust Vision Transformer (RVT). RVT is a new vision transformer, which has superior performance and strong robustness. We further propose two new plug-and-play techniques called position-aware attention rescaling and patch-wise augmentation to train our RVT. The experimental results on ImageNet and six robustness benchmarks show the advanced robustness and generalization ability of RVT compared with previous Transformers and state-of-the-art CNNs. Our RVT-S
 also achieves Top-1 rank on multiple robustness leaderboards including ImageNet-C and ImageNet-Sketch. 

### 框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210602101433.png)

### 结果

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210602101556.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210602101705.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210602101412.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210602101754.png)

### 启示

创新点：position-aware attention rescaling 和 patch-wise augmentation