https://www.arxiv-vanity.com/papers/2104.11746/

We introduce Video Transformer (VidTr) with separable-attention for video classification. Comparing with commonly used 3D networks, VidTr is able to aggregate spatio-temporal information via stacked attentions and provide better performance with higher efficiency. We first introduce the vanilla video transformer and show that transformer module is able to perform spatio-temporal modeling from raw pixels, but with heavy memory usage. We then present VidTr which reduces the memory cost by 3.3 × while keeping the same performance. To further compact the model, we propose the standard deviation based topK pooling attention, which reduces the computation by dropping non-informative features. VidTr achieves state-of-the-art performance on five commonly used dataset with lower computational requirement, showing both the efficiency and effectiveness of our design. Finally, error analysis and visualization show that VidTr is especially good at predicting actions that require long-term temporal reasoning. 

### 框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210601145641.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210601145742.png)

### 结果

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210601145756.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210601145821.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210601145849.png)

### 启示

多层（12层）迭代，创新性不高