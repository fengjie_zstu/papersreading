# MOBILEVIT: LIGHT-WEIGHT, GENERAL-PURPOSE, AND MOBILE-FRIENDLY VISION TRANSFORMER

#Mobile #Transformer

[pdf链接](https://arxiv.org/pdf/2110.02178.pdf)

[源码链接]

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211012154356.png)

## 主要创新点

learn global representations with transformers as convolutions

## 步骤和过程

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211012154549.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211012154636.png)

## 启示