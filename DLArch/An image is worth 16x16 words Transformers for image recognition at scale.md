https://arxiv.org/abs/2010.11929

While the Transformer architecture has become the de-facto standard for natural language processing tasks, its applications to computer vision remain limited. In vision, attention is either applied in conjunction with convolutional networks, or used to replace certain components of convolutional networks while keeping their overall structure in place. We show that this reliance on CNNs is not necessary and a pure transformer applied directly to sequences of image patches can perform very well on image classification tasks. When pre-trained on large amounts of data and transferred to multiple mid-sized or small image recognition benchmarks (ImageNet, CIFAR-100, VTAB, etc.), Vision Transformer (ViT) attains excellent results compared to state-of-the-art convolutional networks while requiring substantially fewer computational resources to train.

### 代码
- https://github.com/google-research/vision_transformer （官方代码）
- https://github.com/emla2805/vision-transformer
- https://github.com/tuvovan/Vision_Transformer_Keras
- https://github.com/ashishpatel26/Vision-Transformer-Keras-Tensorflow-Pytorch-Examples

- https://github.com/lucidrains/vit-pytorch 很多创新思想！！！ 
 @Training data-efficient image transformers & distillation through attention

- https://github.com/jeonsworld/ViT-pytorch 可以查看attention map

### 流程图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222152641.png)

### 性能

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222153602.png)

### 解释文章

https://jacobgil.github.io/deeplearning/vision-transformer-explainability

#### 解释代码

https://github.com/jacobgil/vit-explain

### 启示

实验分析部分很全面

在显著性方面会有更好应用

如何用transformer来进行检测和分割？