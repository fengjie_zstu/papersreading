#UNet

### 结构

unet
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223144508.png)

RCNN Unet
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223144638.png)

Attention Unet
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223144627.png)

Attention-RCNN Unet
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223144612.png)

Nested Unet（Unet++）
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223144600.png)

### 代码

https://github.com/bigmb/Unet-Segmentation-Pytorch-Nest-of-Unets

https://github.com/MrGiovanni/UNetPlusPlus

### 解释

https://zhuanlan.zhihu.com/p/44958351

### 启示

低层与高层特征融合的典范

结构上的创新是一个从0到1的过程，在一个结构上做简单修改是没有多少创新性的

为什么这样设计结构，有时是一时思考，有时是事后总结

unet++对剪枝操作非常友好