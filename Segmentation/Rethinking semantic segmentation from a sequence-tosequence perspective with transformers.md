使用transformers做分割

https://arxiv.org/abs/2012.15840

Most recent semantic segmentation methods adopt a fully-convolutional network (FCN) with an encoder-decoder architecture. The encoder progressively reduces the spatial resolution and learns more abstract/semantic visual concepts with larger receptive fields. Since context modeling is critical for segmentation, the latest efforts have been focused on increasing the receptive field, through either dilated/atrous convolutions or inserting attention modules. However, the encoder-decoder based FCN architecture remains unchanged. In this paper, we aim to provide an alternative perspective by treating semantic segmentation as a sequence-to-sequence prediction task. Specifically, we deploy a pure transformer (ie, without convolution and resolution reduction) to encode an image as a sequence of patches. With the global context modeled in every layer of the transformer, this encoder can be combined with a simple decoder to provide a powerful segmentation model, termed SEgmentation TRansformer (SETR). Extensive experiments show that SETR achieves new state of the art on ADE20K (50.28% mIoU), Pascal Context (55.83% mIoU) and competitive results on Cityscapes. Particularly, we achieve the first (44.42% mIoU) position in the highly competitive ADE20K test server leaderboard.

### 代码 

https://fudan-zvg.github.io/SETR https://github.com/fudan-zvg/SETR（官方，未上传 2021.2）

### 框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222165258.png)

### 结果

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222165413.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222165500.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222165524.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222165727.png)

### 启示

是因为有更好的特征还是学习到了内容上的相关性才有更好的分割效果？