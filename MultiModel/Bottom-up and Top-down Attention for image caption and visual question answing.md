# Bottom-up and Top-down Attention for image caption and visual question answing 笔记 (2018CVPR)
#4
[论文](https://arxiv.org/pdf/1707.07998.pdf)
[代码](https://github.com/peteanderson80/bottom-up-attention)

### 一、摘要
作者提出了bottom-up和top-down两种attention，bottom-up指faster-rcnn提取图片的特征。top-down是指top-down attention lstm网络。

### 二、主要创新点
创新点一：

首次提出用目标检测算法：faster-rcnn来提取图像的特征

创新点二：

提出了一个caption model，里面主要包括 top-down attention lstm、language lstm 

总结构如下图（还有VQA的model，这边省略，有兴趣的自己去看）：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1013/111405_381c3e6a_7615007.png "屏幕截图.png")

#### 1、bottom-up
这部分使用faster rcnn网络来提取图片特征，backbone为ResNet-101。所使用的用来提取特征的模型是重新训练的。作者在ImageNet上训练，随后在Visual Genome上训练。

训练好模型后，提取用于image caption的图片特征经过了下面三个步骤：

第一步是Region Proposal Network，预测object proposals，在每个空间位置上给出一个类别的得分和一个box，通过非极大抑制（nms）和iou阈值来选择最好的几个box proposals，并将其当作第二阶段的输入。

第二步通过region of interest(ROI)从每个box proposal中提取一个14*14的特征图，这些特征图随后batch到一起输入到CNN的最后几层中，最终的输出是在分类的的得分以及每个box proposal的类特定的边界框

第三步处理faster rcnn得到的输出。通过非极大抑制（nms）和iou阈值得到的最终的object class。然后得到这些class对应的box region的mean-pooled convolutional 特征，它的维度是2048。

 **注：** 

为了学习到更好的特征，给原本的标签（只有类别）加入了一些属性描述，如下图：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1020/140320_a0a85926_7615007.png "屏幕截图.png")

为了让网络可以学习到这样的带有属性描述的标签，作者将从第三步得到的mean-pooled convolutional特征与可学习的真实标签的embedding拼接一起，传入另外的输出层，得到分类的softmax分布。

#### 2、top-down attention 
里面就是一层LSTM，输入是三个元素的拼接，如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1020/114030_0621dadd_7615007.png "屏幕截图.png")

其中h是前一时刻language lstm输出，v是k个image feature取均值（比较疑惑这个K怎么来的，batch-size?），WeΠt是词的embedding，We为词库的embedding，Πt为该时刻对应的词的one-hot编码。

x输入LSTM之后，得到ht^1。ht^1 与k个image feature（vi）一起输入 attend，attend就是激活函数加上softmax，公式如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1020/115132_cd5bce6a_7615007.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1020/115213_96a6b9ad_7615007.png "屏幕截图.png")

得到的vt为language LSTM的输入之一
#### 3、language LSTM
language LSTM的输入也有两部分组成，公式如下：，其输出为单词的softmax分布，这样便将图片转化为文本。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1020/154045_a3c44106_7615007.png "屏幕截图.png")

ht^1是attention LSTM的输出，vt为ht经过attend的输出。经过LSTM后得到ht^2，经过线性层后，送入softmax得到t时刻任意单词的概率分布，单词序列（y1--yt），而整句话的概率分布为每个单词概率的乘积，公式如下面所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1020/154846_6a192876_7615007.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1020/154908_bec07a35_7615007.png "屏幕截图.png")

#### 4、其他
除了结构，论文里也写了所使用的loss函数，为交叉熵损失函数，并且论文里提到模型还使用了强化学习的优化方法。
### 三、结果
该结果对于当时来说已经很好了。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1020/155416_ece1f993_7615007.png "屏幕截图.png")

cider optimization 是对cider这个指标进行优化得到的指标。这个结果是在线上测试得到的结果，网站如下：https://competitions.codalab.org/competitions/3221#results

### 四、启示
优异的数据集处理可以给网络减轻很多压力
