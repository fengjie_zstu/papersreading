http://arxiv.org/abs/2202.10936

如何将预训练适应于视觉和语言（V-L）学习领域，并提高下游任务的性能，成为多模态学习的焦点。


VL-PTMs在为文本和图像表征之间的互动建模方面的主流架构：
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307110553.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307110834.png)


fasterrcnn还是主要的图像特征提取手段
