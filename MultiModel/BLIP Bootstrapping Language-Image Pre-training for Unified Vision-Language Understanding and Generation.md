http://arxiv.org/abs/2201.12086

BLIP通过引导caption有效地利用了嘈杂的网络数据，其中一个captioner生成了合成caption，一个过滤器去除了嘈杂的caption。

https://github.com/salesforce/BLIP

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307111826.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307111909.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307111942.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307111959.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307112032.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307112111.png)

