# Auto-encoding and Distilling Scene Graphs for Image Captioning

2019 CVPR 2020 PAMI

#ImageCaption

[pdf链接](https://openaccess.thecvf.com/content_CVPR_2019/papers/Yang_Auto-Encoding_Scene_Graphs_for_Image_Captioning_CVPR_2019_paper.pdf)

[源码链接]

[文章解读](https://zhuanlan.zhihu.com/p/41200392)

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021084232.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021084604.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021084639.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021084704.png)

## 主要创新点

场景图自动编码器（SGAE），将语言归纳偏见纳入编码器-解码器图像说明框架，以获得更像人类的说明。

人类使用归纳偏见来构成话语中的搭配和上下文推断

场景图：一个对象节点由形容词节点和关系节点连接的有向图（G），来表示图像（I）和句子（S）的复杂结构布局

A multi-modal graph convolutional network for modulating scene graphs into visual representations

A SGAE-based encoder-decoder image captioner with a shared dictionary guiding the language decoding

## 步骤和过程

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021084804.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021084834.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021084901.png)

## 启示

如何加入人类的“常识”信息，语言先验

