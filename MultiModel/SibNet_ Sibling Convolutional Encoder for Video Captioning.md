# SibNet: Sibling Convolutional Encoder for Video Captioning

2020 PAMI

#VideoCaption

[pdf链接](https://cse.buffalo.edu/~jsyuan/papers/2018/SibNet__Sibling_Convolutional_Encoder_for_Video_Captioning.pdf)

[源码链接]

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021092354.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021092438.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021092612.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021092638.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021092708.png)


## 主要创新点

提出Sibling Convolutional Encoder (SibNet)，由两个分支组成，内容分支负责捕获视觉信息，语义分支用于生成“特定语义 （semantic-specific）”的表示，这种表示可以捕获某些帧在语义上的重要性，为内容分支提供补充。

设计了新的损失函数，由三项组成，分别是：content loss、semantic loss、decoder loss。

## 步骤和过程

## 结果分析

数据集：
YouTube2Text
MSR-VTT

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021092735.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021092850.png)

## 启示

针对特定问题，设计特定的网络结构，设计特定的loss

自圆其说