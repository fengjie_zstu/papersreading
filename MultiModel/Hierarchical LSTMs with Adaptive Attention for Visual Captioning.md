# Hierarchical LSTMs with Adaptive Attention for Visual Captioning

2019 PAMI

#ImageCaption #LSTM

[pdf链接](http://export.arxiv.org/pdf/1812.11004)

[源码链接]

## 摘要与网络结构框图

大多数现有的解码器将注意力机制应用于每个生成的词，包括视觉词（如 "枪 "和 "射击"）和非视觉词（如 "the"，"a"）。然而，这些非视觉词汇可以很容易地使用自然语言模型进行预测，而不考虑视觉信号或注意力。将注意力机制强加于非视觉性词语可能会误导并降低视觉字幕的整体性能。

此外，LSTM的层次结构使得视觉数据的表示更加复杂，可以捕捉到不同尺度的信息。考虑到这些问题，提出了一种带有自适应注意力的分层LSTM（hLSTMat）方法，用于图像和视频字幕。具体来说，所提出的框架利用空间或时间注意力来选择特定的区域或帧来预测相关的词，而自适应注意力则用于决定是否依赖视觉信息或语言背景信息。同时，设计了一个分层的LSTMs来同时考虑低层次的视觉信息和高层次的语言环境信息来支持字幕的生成。

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020142242.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020142332.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020142400.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020142433.png)


## 主要创新点

视觉注意力更关注“实词”，忽略 a the等

分层LSTM

## 步骤和过程

## 结果分析

视频数据集：
The Microsoft Video Description Corpus (MSVD). 
MSR Video to Text (MSR-VTT). 
Large Scale Movie Description Challenge (LSMDC).

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020142556.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020142638.png)

## 启示

视频数据集

针对问题特定的网络结构