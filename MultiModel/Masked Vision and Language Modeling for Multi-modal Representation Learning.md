
https://arxiv.org/abs/2208.02131

本文研究了如何将掩蔽信号建模应用于视觉和语言（V+L）表征学习。 本文提出了一种联合的掩蔽视觉和语言建模方法，即在一种模态的掩蔽信号的基础上，利用另一种模态的掩蔽信号来重建掩蔽的视觉和语言模型，而不是单独开发掩蔽语言建模和掩蔽图像建模。 这是由图像—文本配对数据的性质所激发的，即图像和文本两者以不同的格式传达几乎相同的信息。 以另一模态为条件的一个模态的掩蔽信号重建也可以隐式地学习语言标记和图像块之间的跨模态对准。 在多种V+L任务上的实验结果表明，该方法不仅在使用大量数据的情况下取得了最佳的性能，而且在训练数据有限的情况下也显著优于其他竞争者.

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824145647.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824145736.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824145802.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824145833.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824145932.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824150000.png)

## 启示

以另一模态为条件的一个模态的掩蔽信号重建

如何mask？