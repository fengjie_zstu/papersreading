## 中英文视频数据集 VATEX v1.1

https://eric-xw.github.io/vatex-website/download.html

```
{
    'videoID': 'YouTubeID_StartTime_EndTime',
    'enCap': 
        [
            'Regular English Caption #1',
            'Regular English Caption #2',
            'Regular English Caption #3',
            'Regular English Caption #4',
            'Regular English Caption #5',
            'Parallel English Caption #1',
            'Parallel English Caption #2',
            'Parallel English Caption #3',
            'Parallel English Caption #4',
            'Parallel English Caption #5'
        ],
    'chCap': 
        [
            'Regular Chinese Caption #1',
            'Regular Chinese Caption #2',
            'Regular Chinese Caption #3',
            'Regular Chinese Caption #4',
            'Regular Chinese Caption #5',
            'Parallel Chinese Caption #1',
            'Parallel Chinese Caption #2',
            'Parallel Chinese Caption #3',
            'Parallel Chinese Caption #4',
            'Parallel Chinese Caption #5'
        ]
}
```

视频I3D特征，标注已下载：
链接: https://pan.baidu.com/s/1GxXOI07e9Fe9-0Rfw2zfyQ 提取码: 7jcq 

视频下载工具：
Due to the legal and privacy concerns, we cannot directly share the downloaded videos or clips from YouTube in any way (including but not limited to email, online drives and GitHub). However, there are many open-source tools to download the original clips (e.g., [[Tool #1]](https://github.com/activitynet/ActivityNet/tree/master/Crawler/Kinetics) and [[Tool #2]](https://github.com/ytdl-org/youtube-dl)). Some videos might be unavailable (deleted or hidden by either YouTube or the users) at this moment, but they were available when we collected the dataset. Considering that it is an extremely small percentage, we expect that it won't have a significant impact on the performance.

---



