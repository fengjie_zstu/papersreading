
https://arxiv.org/abs/2203.00386

we propose a self-supervised scheme named as CLIP-GEN for general text-to-image generation with the language-image priors extracted with a pre-trained CLIP model

利用预训练模型生成一些语言-图像对

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307105723.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307105835.png)

image encoder of CLIP

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307105923.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307105950.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307110203.png)

启示：
用CLIP作为image-caption的特征提取器？

code：
Biggan+clip. https://github.com/lucidrains/ big-sleep. 
Deepdaze. https://github.com/lucidrains/ deep-daze. 
Diffusion modal+clip. https : / / github . com / nerdyrodent / CLIP - Guided - Diffusion. 
Magnet. https : / / github . com / robgon - art / MAGnet. 
Siren+fft+clip. https : / / github . com / eps696 / aphantasia. 
triangle+clip. https : / / github . com / google / brain-tokyo-workshop/. 
Vqgan+clip. https://github.com/nerdyrodent/ VQGAN-CLIP.
