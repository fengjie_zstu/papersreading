# xGQA: Cross-Lingual Visual Question Answering

#QA

[pdf链接](https://arxiv.org/pdf/2109.06082.pdf)

[源码链接](https://github.com/Adapter-Hub/xGQA)

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008100927.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008101329.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008101826.png)

## 主要创新点

多语言问答方面提出了一个基本模型框架和实验手段

## 步骤和过程

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008101443.png)

## 启示

多模态模型的简单跨语言转移会产生潜在的多语言多模态的错位，需要更复杂的视觉和多语言建模的方法。
针对多语言的图像描述是否也有同样的问题，这个方向还是很有创新性的。