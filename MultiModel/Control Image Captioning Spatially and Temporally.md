# Control Image Captioning Spatially and Temporally

#ImageCaption

[pdf链接](https://aclanthology.org/2021.acl-long.157.pdf) 

## 摘要与网络结构框图

根据用户意图生成图像描述

将鼠标痕迹作为图像描述任务的另一个输入，让用户控制图像描述的内容。

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211009095203.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211009095237.png)


## 主要创新点

如何有效地利用痕迹来提高生成描述的质量和可控性

新模型：LoopCAG

将对比性约束和注意引导，以环形方式参与明确的空间和时间约束的生成过程。
每个生成的句子都通过对比学习策略在时间上与相应的痕迹序列相一致。

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211009095323.png)

## 步骤和过程

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211009095438.png)

## 启示

更面向实际的图像描述方式

更多约束，会得到更好的效果

如何用算法（loss）去描述这些约束是优化的关键

