# Multimodal Transformer with Multi-View Visual Representation for Image Captioning

2019 CSVT 杭电 计算机学院院长

#ImageCaption

[pdf链接](https://arxiv.org/pdf/1905.07841.pdf)

[源码链接]

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020170153.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020170231.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020170323.png)

## 主要创新点

受到Transformer模型在机器翻译中的启发，将其扩展到用于图像字幕的多模态Transformer（MT）模型。与现有的图像说明方法相比，MT模型在一个统一的注意力区块中同时捕捉到了模式内和模式间的相互作用。

这种注意块的深度模块化构成，MT模型可以进行复杂的多模态推理并输出准确的标题。

多视图视觉特征被无缝地引入到MT模型中

## 步骤和过程

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020170409.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020170511.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020170543.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020170607.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211020170627.png)

## 启示

多模型提取特征--相当于提供更多信息，难点：如何把各种信息融合起来

