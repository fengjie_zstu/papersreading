https://arxiv.org/abs/2101.10804

In this paper, we consider the image captioning task from a new sequence-to-sequence prediction perspective and propose CaPtion TransformeR (CPTR) which takes the sequentialized raw images as the input to Transformer. Compared to the "CNN+Transformer" design paradigm, our model can model global context at every encoder layer from the beginning and is totally convolution-free. Extensive experiments demonstrate the effectiveness of the proposed model and we surpass the conventional "CNN+Transformer" methods on the MSCOCO dataset. Besides, we provide detailed visualizations of the self-attention between patches in the encoder and the "words-to-patches" attention in the decoder thanks to the full Transformer architecture.

### 全transformer图像+文字现状

DETR [5] utilizes Transformer to decode detection predictions without prior knowledge such as region proposals and non-maximal suppression.

ViT [6] firstly utilizes Transformer without any applications of convolution operation for image classification and shows promising performance especially when pretrained on very huge datasets.

full Transformer methods for both high-level and low-level down-stream tasks emerge, such as SETR [7] for image semantic segmentation and IPT [8] for image processing.

#### 引用 

[5] Nicolas Carion, Francisco Massa, Gabriel Synnaeve, Nicolas Usunier, Alexander Kirillov, and Sergey Zagoruyko, “End-to-end object detection with transformers,” arXiv preprint arXiv:2005.12872, 2020.
[End-to-end object detection with transformers]

[6] Alexey Dosovitskiy, Lucas Beyer, Alexander Kolesnikov, Dirk Weissenborn, Xiaohua Zhai, Thomas Unterthiner, Mostafa Dehghani, Matthias Minderer, Georg Heigold, Sylvain Gelly, et al., “An image is worth 16x16 words: Transformers for image recognition at scale,” arXiv preprint arXiv:2010.11929, 2020.
[An image is worth 16x16 words: Transformers for image recognition at scale]

[7] Sixiao Zheng, Jiachen Lu, Hengshuang Zhao, Xiatian Zhu, Zekun Luo, Yabiao Wang, Yanwei Fu, Jianfeng Feng, Tao Xiang, Philip HS Torr, et al., “Rethinking semantic segmentation from a sequence-tosequence perspective with transformers,” arXiv preprint arXiv:2012.15840, 2020.
[Rethinking semantic segmentation from a sequence-tosequence perspective with transformers]

[8] Hanting Chen, Yunhe Wang, Tianyu Guo, Chang Xu, Yiping Deng, Zhenhua Liu, Siwei Ma, Chunjing Xu, Chao Xu, and Wen Gao, “Pre-trained image processing transformer,” arXiv preprint arXiv:2012.00364, 2020
[Pre-trained image processing transformer]

### 使用全transformer的优势
encoder of CPTR can utilize long-range dependencies among the sequentialized patches from the very beginning via self-attention mechanism

### 流程图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222151051.png)

### 性能

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222151238.png)

### 启示

没有太大创新，关键在于将ViT的思想和后面的Transformer连接起来