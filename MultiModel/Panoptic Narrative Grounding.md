# Panoptic Narrative Grounding

#ImageCaption

[pdf链接](https://arxiv.org/pdf/2109.04988.pdf)

[源码链接](https://github.com/BCV-Uniandes/PNG)

## 摘要与网络结构框图

细粒度的图片描述
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008111237.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008111424.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008111522.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008111651.png)

## 主要创新点
基于全景分割，为描述中的每个名词短语提供密集的依据

## 步骤和过程

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008111610.png)

## 启示
精细化的图像描述方向，基于分割

根据描述反向指导分割