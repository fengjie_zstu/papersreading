#knowledge

https://arxiv.org/abs/2103.15365

https://github.com/thunlp/VisualDS

### 缘起
场景图的生成旨在识别图像中的物体及其关系，提供结构化的图像表示

### 创新
通过对齐常识性知识库和图像，我们可以自动创建大规模的标记数据，为视觉关系学习提供远距离监督

### 结构
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916153405.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916153446.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210916153542.png)