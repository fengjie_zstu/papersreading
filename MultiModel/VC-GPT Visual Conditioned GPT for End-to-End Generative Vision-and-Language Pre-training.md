http://arxiv.org/abs/2201.12723

一个新的G-VLP框架，视觉条件GPT（VC-GPT），并用一个小规模的图像标题语料库（视觉基因组，只有11万张不同的图像）进行预训练

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307120846.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307120920.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307120952.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307121017.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220307121036.png)

