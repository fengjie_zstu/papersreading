# On Diversity in Image Captioning: Metrics and Methods

2019 CVPR

#ImageCaption #Metrics

[pdf链接](https://openaccess.thecvf.com/content_CVPR_2019/papers/Wang_Describing_Like_Humans_On_Diversity_in_Image_Captioning_CVPR_2019_paper.pdf)

[源码链接]

[解读](https://blog.csdn.net/luo3300612/article/details/90899379)

## 摘要与网络结构框图

提出了一个衡量一组字幕多样性的指标，该指标来自于潜在语义分析（LSA），然后使用CIDEr相似性对LSA进行内核化。
与mBLEU相比，提出的多样性指标显示出与人类评价相对较强的关联性。

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021091343.png)

## 主要创新点

一张图片胜过千言万语，图片中包括了各种不同的概念，因此对它的描述是多样性的，但仅仅注重accuracy会导致模型产生常见的短语来避免错误
diversity可以度量captioner的variance

本文设计了一个度量image caption模型的diversity的制表

## 步骤和过程

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211021091507.png)

## 启示

从度量方法角度上看描述结果

描述评价是多个语句，多样性衡量标准（基于计算值（或特征值）组成的核心矩阵）