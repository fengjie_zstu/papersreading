# AUDIO-VISUAL SPEECH RECOGNITION IS WORTH 32×32×8 VOXELS

#Audio

https://arxiv.org/pdf/2109.09536.pdf


## 摘要与网络结构框图
将视觉信息（读唇）融入到语音识别中

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008100055.png)

## 主要创新点
1. 使用基于Transformer架构的设计（视频信息的transformer）
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008100356.png)

2. 对比读唇任务

## 结果分析
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008100456.png)

## 启示
相当于增加了信息，用以辅助语音识别