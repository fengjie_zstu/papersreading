# Cross-Attention is All You Need: Adapting Pretrained Transformers for Machine Translation

#Translate

[pdf链接](https://arxiv.org/pdf/2104.08771.pdf)

https://github.com/MGheini/xattn-transfer-for-mt

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211008102140.png)

## 主要创新点

交叉注意力模型

## 步骤和过程

## 结果分析

## 启示
借鉴机器翻译论文中模型的思想，应用到图像描述中，因为图像描述本质上也是一个域到另一个域的转换
如何构造数据集？