
https://arxiv.org/abs/2112.09133

提出了一种用于视频模型自监督预训练的掩蔽特征预测（MaskFeat）算法. 我们的方法首先随机屏蔽掉输入序列的一部分，然后预测屏蔽区域的特征。 
我们研究了五种不同类型的特征，发现方向梯度直方图（HOG），一种手工制作的特征描述符，在性能和效率方面都表现得特别好。 我们观察到HOG中的局部对比度归一化对于良好的结果是必不可少的，这与早期使用HOG进行视觉识别的工作一致。 该方法可以学习丰富的视觉知识，并驱动大规模的基于Transformer的模型。 无需使用额外的模型权重或监督，在未标记视频上预训练的MaskFeat在Kinetics-400、Kinetics-600、Kinetics-700、AVA和SSv2上获得了前所未有的结果：使用MViT-L获得了86.7%、88.3%、80.4%、38.8 mAP和75.0%。 MaskFeat进一步推广到图像输入，它可以被解释为具有单帧的视频，并在ImageNet上获得有竞争力的结果。

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824150312.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824150337.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824150418.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824150516.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824150537.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824150622.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20220824150650.png)

## 启示

HOG？

生成简单特征而不是像素信息，再由简单特征恢复？