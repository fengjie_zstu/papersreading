https://arxiv.org/abs/2005.12872v3

使用transformers做检测+分割

We present a new method that views object detection as a direct set prediction problem. Our approach streamlines the detection pipeline, effectively removing the need for many hand-designed components like a non-maximum suppression procedure or anchor generation that explicitly encode our prior knowledge about the task. The main ingredients of the new framework, called DEtection TRansformer or DETR, are a set-based global loss that forces unique predictions via bipartite matching, and a transformer encoder-decoder architecture. Given a fixed small set of learned object queries, DETR reasons about the relations of the objects and the global image context to directly output the final set of predictions in parallel. The new model is conceptually simple and does not require a specialized library, unlike many other modern detectors. DETR demonstrates accuracy and run-time performance on par with the well-established and highly-optimized Faster RCNN baseline on the challenging COCO object detection dataset. Moreover, DETR can be easily generalized to produce panoptic segmentation in a unified manner. We show that it significantly outperforms competitive baselines. 

### 代码

- https://github.com/facebookresearch/detr （官方）


### 框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222162842.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222163224.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222163759.png)

分割

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222163527.png)

### 性能

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222163319.png)

分割
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222163609.png)

### 启示

backbone还是一样，提取的图像特征接transformer，如何接？如何设计loss？看代码【TODO】