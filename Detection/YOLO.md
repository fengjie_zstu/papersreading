#YOLO

### 性能

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223101801.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210224170904.png)

### 代码

https://github.com/amusi/YOLO-Reproduce-Summary

https://github.com/wuzhihao7788/yolodet-pytorch
各个系列

#### v3

https://github.com/eriklindernoren/PyTorch-YOLOv3

https://github.com/westerndigitalcorporation/YOLOv3-in-PyTorch

#### v4

[[YOLOv4- Optimal Speed and Accuracy of Object Detection.pdf]]

*https://github.com/Tianxiaomo/pytorch-YOLOv4*
非常完整，从pytorch到onnx到trt

https://github.com/VCasecnikovs/Yet-Another-YOLOv4-Pytorch
实现yolov4的各种算法

https://github.com/bubbliiiing/yolov4-tiny-pytorch
自己实现，有faq

Scaled-YOLOv4: Scaling Cross Stage Partial Network  https://arxiv.org/abs/2011.08036 
https://alexeyab84.medium.com/scaled-yolo-v4-is-the-best-neural-network-for-object-detection-on-ms-coco-dataset-39dfa22fa982
https://github.com/WongKinYiu/ScaledYOLOv4 https://github.com/Eric3911/ScaledYOLOv4

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210224170648.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210224171152.png)

#### 代码阅读

- 模型结构

https://github.com/Tianxiaomo/pytorch-YOLOv4/blob/master/models.py

按照backbone，neck，head顺序，非常清楚

https://github.com/bubbliiiing/yolov4-tiny-pytorch/blob/master/nets/CSPdarknet53_tiny.py

自己实现的结构，有图有说明

- Loss

https://github.com/Tianxiaomo/pytorch-YOLOv4/blob/master/train.py 中的 Yolo_loss 

https://github.com/bubbliiiing/yolov4-tiny-pytorch/blob/master/nets/yolo_training.py 

### 解释

吴恩达对yolo的解释 https://www.youtube.com/watch?v=9s_FpMpdYW8

其他讲解 v1-v5 https://www.bilibili.com/video/BV1kv4y1Z7eR

带代码的讲解 https://www.bilibili.com/video/BV1Hp4y1y788【TODO】

https://medium.com/m/global-identity?redirectUrl=https://heartbeat.fritz.ai/introduction-to-yolov4-research-review-5b6b4bd5f255

论文 https://arxiv.org/pdf/2004.10934.pdf
There are a huge number of features which are said to improve Convolutional Neural Network (CNN) accuracy. Practical testing of combinations of such features on large datasets, and theoretical justification of the result, is required. Some features operate on certain models exclusively and for certain problems exclusively, or only for small-scale datasets; while some features, such as batch-normalization and residual-connections, are applicable to the majority of models, tasks, and datasets. We assume that such universal features include Weighted-Residual-Connections (WRC), Cross-Stage-Partial-connections (CSP), Cross mini-Batch Normalization (CmBN), Self-adversarial-training (SAT) and Mish-activation. We use new features: WRC, CSP, CmBN, SAT, Mish activation, Mosaic data augmentation, CmBN, DropBlock regularization, and CIoU loss, and combine some of them to achieve state-of-the-art results: 43.5% AP (65.7% AP50) for the MS COCO dataset at a realtime speed of ~65 FPS on Tesla V100.

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223103547.png)

- *backbone* CSPDarknet53. CSP stands for Cross-Stage-Partial connections.

目的在于提取特征，对于边缘设备上的应用，也可以使用mobilenet之类的网络

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223102556.png)

- *neck* Path Aggregation Network (PANet) & Spatial Attention Module (SAM) & Spatial Pyramid Pooling (SPP)

目的在于在backbone和head（稠密预测层）之间提供额外层

PANet
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223103517.png)

SAM
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223103800.png)

SPP
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223103925.png)

- *head* 

目的在于获得框位置与框内类别，与之前的yolo类似

- Bag-Of-Freebies

backbone技巧
> CutMix and Mosaic data augmentation, DropBlock regularization, Class label smoothing.

detector技巧
> CIoU-loss, CmBN, DropBlock regularization, Mosaic data augmentation, self-adversarial training, eliminate grid sensitivity, using multiple anchors for a single ground-truth sample, cosine annealing scheduler, optimizing hyperparameters, random training shapes.

DropBlock:
![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223110035.png)

- Bag-Of-Specials

改变网络架构

backbone改进
> Mish activation, cross-stage partial connections (CSP), multi-input weighted residual connections (MiWRC)

detector改进
> Mish activation, SPP-block, SAM-block, PAN path-aggregation block, DIoU-NMS


### 转换

https://github.com/linghu8812/tensorrt_inference/tree/master/ScaledYOLOv4

### 启示

yolov4优化方法的集大成者，类似h.26x视频编码算法的改进，每个部分都进行提升，整体效果会有很大的提升。
增加训练的复杂度，保持推理的复杂度。类似与视频编码与解码。