# Faster-RCNN

#detection

https://arxiv.org/abs/1506.01497

源码链接



解读

![输入图片说明](https://images.gitee.com/uploads/images/2021/1106/085120_d117e91a_8933477.png "1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1106/085131_cfabd538_8933477.png "2.png")

## 摘要与网络结构框图

State-of-the-art object detection networks depend on region proposal algorithms to hypothesize object locations.
Advances like SPPnet [1] and Fast R-CNN [2] have reduced the running time of these detection networks, exposing region
proposal computation as a bottleneck. In this work, we introduce a Region Proposal Network (RPN) that shares full-image
convolutional features with the detection network, thus enabling nearly cost-free region proposals. An RPN is a fully convolutional network that simultaneously predicts object bounds and objectness scores at each position. The RPN is trained end-to-end to generate high-quality region proposals, which are used by Fast R-CNN for detection. We further merge RPN and Fast R-CNN into a single network by sharing their convolutional features—using the recently popular terminology of neural networks with “attention” mechanisms, the RPN component tells the unified network where to look. For the very deep VGG-16 model [3], our detection system has a frame rate of 5fps (including all steps) on a GPU, while achieving state-of-the-art object detection accuracy on PASCAL VOC 2007, 2012, and MS COCO datasets with only 300 proposals per image. In ILSVRC and COCO
2015 competitions, Faster R-CNN and RPN are the foundations of the 1st-place winning entries in several tracks. Code has been
made publicly available.

![输入图片说明](https://images.gitee.com/uploads/images/2021/1106/085213_23db6f3b_8933477.png "3.png")



## 主要创新点

* 在生成建议框时，利用RPN(Region Proposal Network)替代SS（Selective Search）
* 产生建议窗口的CNN和目标检测的CNN共享

## 步骤和过程

1,输入图片

2,图片进入CNN，包括卷积，池化和激活函数（relu），进行特征提取，生成feature map

3,将2中生成的featrue map 经过3*3卷积进入RPN中，在RPN中生成锚框（Archor box）,对其进行裁剪过滤后通过softmax判断anchors属于前景(foreground)或者后景(background)，即是物体or不是物体，所以这是一个二分类；同时，另一分支bounding box regression修正anchor box，形成较精确的proposal（注：这里的较精确是相对于后面全连接层的再一次box regression而言）

4, 将生成的建议窗口映射到featrue map中，通过RoI pooling层使每个RoI生成固定尺寸的feature map

5, 利用Softmax进行分类训练和利用Smooth L1 Loss(探测边框回归)进行边框回归(Bounding box regression)

## 结果分析



## 启示

可利用消融实验来确定每一部分对于整个框架的重要性

使用RPN不仅可以提高建议框的质量，而且还能提高选框准确性。

是在Fast-RCNN的基础上更前进一步

