https://arxiv.org/abs/2010.04159

DETR has been recently proposed to eliminate the need for many hand-designed components in object detection while demonstrating good performance. However, it suffers from slow convergence and limited feature spatial resolution, due to the limitation of Transformer attention modules in processing image feature maps. To mitigate these issues, we proposed Deformable DETR, whose attention modules only attend to a small set of key sampling points around a reference. Deformable DETR can achieve better performance than DETR (especially on small objects) with 10 times less training epochs. Extensive experiments on the COCO benchmark demonstrate the effectiveness of our approach. 

### 代码

https://github.com/fundamentalvision/Deformable-DETR

### 框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223090215.png)
In Deformable DETR , we utilize (multi-scale) deformable attention modules to replace the Transformer attention modules processing feature maps

### 结果

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223090559.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210223090718.png)

### 启示

如何利用transformer有效获取多尺度特征？