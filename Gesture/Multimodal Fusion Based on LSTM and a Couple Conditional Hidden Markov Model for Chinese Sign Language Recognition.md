# Multimodal Fusion Based on LSTM and a Couple Conditional Hidden Markov Model for Chinese Sign Language Recognition

#Gesture

[pdf链接](https://ieeexplore.ieee.org/ielx7/6287639/8600701/08750875.pdf)


## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022150322.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022150345.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022150446.png)

## 主要创新点

Dual long short-term memory (LSTM) and a couple hidden Markov model (CHMM)

## 步骤和过程

自制数据集

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022151119.png)

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022150549.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022150606.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022150628.png)

## 启示

多信息融合是写论文的王道，但不是做实际项目的做法