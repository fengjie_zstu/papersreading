# An attention enhanced graph convolutional lstm network for skeleton-based action recognition

2019 CVPR

#Graph #LSTM #Gesture

[pdf链接](https://openaccess.thecvf.com/content_CVPR_2019/papers/Si_An_Attention_Enhanced_Graph_Convolutional_LSTM_Network_for_Skeleton-Based_Action_CVPR_2019_paper.pdf)

[源码链接]

[解读](https://blog.csdn.net/u014386899/article/details/103102163)

[解读](https://zhuanlan.zhihu.com/p/181402451)

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022140548.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022140629.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022140742.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022140815.png)

## 主要创新点

注意力增强图卷积LSTM网络（AGC-LSTM）

不仅可以捕捉到空间配置和时间动态的辨别特征，而且还可以探索空间和时间领域的共现关系。

时间分层架构，以增加顶部AGC-LSTM层的时间感受野，这提高了学习高层语义表征的能力，并大大降低了计算成本。

为了选择有鉴别力的空间信息，采用了注意力机制来增强每个AGC-LSTM层中关键关节的信息。

## 步骤和过程

## 结果分析

数据集：
NTU RGB+D dataset 
NorthwesternUCLA dataset

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022140846.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022140912.png)


## 启示

用于手语识别！！！