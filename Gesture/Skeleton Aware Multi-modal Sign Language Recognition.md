# Skeleton Aware Multi-modal Sign Language Recognition

CVPR 2021

#Gesture

[pdf链接](https://openaccess.thecvf.com/content/CVPR2021W/ChaLearn/papers/Jiang_Skeleton_Aware_Multi-Modal_Sign_Language_Recognition_CVPRW_2021_paper.pdf)

[源码链接](https://github.com/jackyjsy/CVPR21Chal-SLR)

## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022144342.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022144420.png)

## 主要创新点

骨架感知多模态SLR框架（SAM-SLR），以利用多模态信息实现更高的识别率

手语图卷积网络（SL-GCN） —— 模拟嵌入式动态

可分离空间-时间卷积网络（SSTCN）—— 骨架特征

RGB和深度模式 —— 提供与基于骨架的SL-GCN和SSTCN方法互补的全局信息

## 步骤和过程

## 结果分析

## 启示

多模型，复杂度太高，将其中的SLGCN提取出来做个简化版本？