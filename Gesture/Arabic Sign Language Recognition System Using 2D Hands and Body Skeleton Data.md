# Arabic Sign Language Recognition System Using 2D Hands and Body Skeleton Data

IEEE Access 2021

#Gesture

[pdf链接](https://ieeexplore.ieee.org/ielx7/6287639/6514899/09389720.pdf)


## 摘要与网络结构框图

整个系统流程，包括数据采集

## 主要创新点

## 步骤和过程

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022145722.png)


## 结果分析

手势种类少

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022145558.png)

## 启示

Access质量参差不齐