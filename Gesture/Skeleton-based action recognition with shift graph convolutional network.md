# Skeleton-based action recognition with shift graph convolutional network

CVPR 2020

#Gesture #Graph #TODO

[pdf链接](https://openaccess.thecvf.com/content_CVPR_2020/papers/Cheng_Skeleton-Based_Action_Recognition_With_Shift_Graph_Convolutional_Network_CVPR_2020_paper.pdf)

[源码链接](https://github.com/kchengiva/Shift-GCN)

[解读1](https://blog.csdn.net/qq_33331451/article/details/106860828)
[解读2](https://blog.csdn.net/LoveKKarlie_/article/details/115536729)
[解读3](https://blog.csdn.net/hahameier/article/details/109657562)


## 摘要与网络结构框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022143129.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022143213.png)


## 主要创新点

移位图卷积网络（Shift-GCN）

Shift-GCN不是使用沉重的常规图形卷积，而是由新颖的移位图形操作和轻量级的点-明智卷积组成，其中移位图形操作为空间图形和时间图形提供了灵活的感受野

计算复杂度降低

## 步骤和过程

## 结果分析

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022143054.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20211022143250.png)

## 启示

如何降低计算量也是需要考虑的问题