#表格识别 

https://arxiv.org/abs/2303.03730v1

Table structure recognition (TSR) aims at extracting tables in images into machine-understandable formats. Recent methods solve this problem by predicting the adjacency relations of detected cell boxes, or learning to generate the corresponding markup sequences from the table images. However, they either count on additional heuristic rules to recover the table structures, or require a huge amount of training data and time-consuming sequential decoders. In this paper, we propose an alternative paradigm. We model TSR as a logical location regression problem and propose a new TSR framework called LORE, standing for LOgical location REgression network, which for the first time combines logical location regression together with spatial location regression of table cells. Our proposed LORE is conceptually simpler, easier to train and more accurate than previous TSR models of other paradigms. Experiments on standard benchmarks demonstrate that LORE consistently outperforms prior arts. Code is available at https:// [this http URL](http://github.com/AlibabaResearch/AdvancedLiterateMachinery/tree/main/DocumentUnderstanding/LORE-TSR).  
表格结构识别（TSR）的目标是将图像中的表格提取为机器可理解的格式。最近的方法通过预测检测到的单元格框的邻接关系或学习从表格图像生成对应的标记序列来解决这个问题。然而，它们要么依赖额外的启发式规则来恢复表结构，要么需要大量的训练数据和耗时的顺序解码器。在本文中，我们提出了一个替代范式。将TSR建模为逻辑位置回归问题，提出了一个新的TSR框架LORE，即逻辑位置回归网络，首次将逻辑位置回归与表格单元格的空间位置回归结合起来.我们提出的LORE比以往其他范式的TSR模型概念上更简单、更容易训练、更准确。在标准基准上的实验证明LORE始终优于现有技术。

![[Pasted image 20230321181003.png]]

![[Pasted image 20230321181022.png]]
![[Pasted image 20230321181042.png]]

![[Pasted image 20230321181112.png]]

