#transformer #多任务处理

https://arxiv.org/abs/2012.00364

As the computing power of modern hardware is increasing strongly, pre-trained deep learning models (e.g., BERT, GPT-3) learned on large-scale datasets have shown their effectiveness over conventional methods. The big progress is mainly contributed to the representation ability of transformer and its variant architectures. In this paper, we study the low-level computer vision task (e.g., denoising, super-resolution and deraining) and develop a new pre-trained model, namely, image processing transformer (IPT). To maximally excavate the capability of transformer, we present to utilize the well-known ImageNet benchmark for generating a large amount of corrupted image pairs. The IPT model is trained on these images with multi-heads and multi-tails. In addition, the contrastive learning is introduced for well adapting to different image processing tasks. The pre-trained model can therefore efficiently employed on desired task after fine-tuning. With only one pre-trained model, IPT outperforms the current state-of-the-art methods on various low-level benchmarks.

#### 框图

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222170307.png)

#### 结果

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222170217.png)

![](https://arloseimg.oss-cn-hangzhou.aliyuncs.com/20210222170616.png)

#### 启示

transformer玩出花，重点在于如何设计，将一个新的网络框架应用到其他领域。

从信息的角度来看，transformer在图像上的应用重点在于可以看到图像的全局信息，这是基于CNN的方法在感受野方面的缺陷，使用transformer可以大大解决。

文章附录C讨论了多任务的情况：
the pre-trained model using multi-task training is better than that of singletask training for about 0.3dB, which suggests the multi-task training would learn universal representation of image processing tasks.
多任务比单任务更好，是因为多任务可以从不同角度去看待图像信息，这样也给图像信息提取本身带来了优势。