#去雾 

Video dehazing aims to recover haze-free frames with high visibility and contrast. This paper presents a novel framework to effectively explore the physical haze priors and aggregate temporal information. Specifically, we design a memory-based physical prior guidance module to encode the prior-related features into long-range memory. Besides, we formulate a multi-range scene radiance recovery module to capture space-time dependencies in multiple space-time ranges, which helps to effectively aggregate temporal information from adjacent frames. Moreover, we construct the first large-scale outdoor video dehazing benchmark dataset, which contains videos in various real-world scenarios. Experimental results on both synthetic and real conditions show the superiority of our proposed method.  
视频去雾的目的是恢复无雾的帧，使其具有较高的可见度和对比度。本文提出了一种新的框架来有效地探索物理霾先验和聚合时间信息。具体而言，我们设计了一个基于记忆的物理先验引导模块，将先验相关特征编码到长距离记忆中。此外，我们还建立了一个多距离场景辐射恢复模型，以捕捉多个时空范围内的时空依赖性，从而有效地聚合相邻帧的时间信息。此外，我们还构建了第一个大规模户外视频去雾基准数据集，其中包含了各种真实场景下的视频。仿真实验和真实的环境下的实验结果表明了该方法的优越性。

https://arxiv.org/abs/2303.09757v1

https://github.com/jiaqixuac/MAP-Net

![[Pasted image 20230321072903.png]]

![[Pasted image 20230321072924.png]]

![[Pasted image 20230321072954.png]]

![[Pasted image 20230321073019.png]]

![[Pasted image 20230321073043.png]]

#时域对齐
#基于记忆的模块



