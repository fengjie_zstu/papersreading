#表格识别

https://arxiv.org/abs/2303.00716v1

Benchmark datasets for table structure recognition (TSR) must be carefully processed to ensure they are annotated consistently. However, even if a dataset's annotations are self-consistent, there may be significant inconsistency across datasets, which can harm the performance of models trained and evaluated on them. In this work, we show that aligning these benchmarks—removing both errors and inconsistency between them—improves model performance significantly. We demonstrate this through a data-centric approach where we adopt a single model architecture, the Table Transformer (TATR), that we hold fixed throughout. Baseline exact match accuracy for TATR evaluated on the ICDAR-2013 benchmark is 65% when trained on PubTables-1M, 42% when trained on FinTabNet, and 69% combined. After reducing annotation mistakes and inter-dataset inconsistency, performance of TATR evaluated on ICDAR-2013 increases substantially to 75% when trained on PubTables-1M, 65% when trained on FinTabNet, and 81% combined. We show through ablations over the modification steps that canonicalization of the table annotations has a significantly positive effect on performance, while other choices balance necessary trade-offs that arise when deciding a benchmark dataset's final composition. Overall we believe our work has significant implications for benchmark design for TSR and potentially other tasks as well. All dataset processing and training code will be released.  
必须仔细处理用于表结构识别（TSR）的基准数据集，以确保它们的注释一致。然而，即使数据集的注释是自一致的，数据集之间也可能存在显著的不一致性，这可能会损害在数据集上训练和评估的模型的性能。在这项工作中，我们表明，对齐这些基准 消除它们之间的错误和不一致 显着提高模型性能。我们通过一种以数据为中心的方法来演示这一点，在这种方法中，我们采用了一个单一的模型体系结构，即表转换器（TATR），我们始终认为它是固定的。当在PubTables-1 M上训练时，在ICDAR-2013基准上评价的TATR的基线精确匹配准确度为65%，当在FinTabNet上训练时为42%，组合为69%。 在减少注释错误和数据集间不一致性后，在PubTables-1 M上训练时，ICDAR-2013上评价的TATR性能大幅增加至75%，在FinTabNet上训练时为65%，合并后为81%。我们通过对修改步骤的分析表明，表注释的规范化对性能有显著的积极影响，而其他选择可以平衡在决定基准数据集的最终组成时出现的必要权衡。总的来说，我们相信我们的工作对TSR的基准设计和潜在的其他任务都有重要的意义。将发布所有数据集处理和培训代码。

![[Pasted image 20230321181252.png]]

![[Pasted image 20230321181311.png]]

![[Pasted image 20230321181353.png]]
