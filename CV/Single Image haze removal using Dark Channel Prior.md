#去雾

##### pdf链接：

https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=5206515&tag=1

##### 源码：

```python
import cv2;
import math;
import numpy as np
import matplotlib.pyplot as plt

def DarkChannel(im,sz):
    b,g,r = cv2.split(im)
    dc = cv2.min(cv2.min(r,g),b);
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(sz,sz))
    dark = cv2.erode(dc,kernel)
    return dark

def AtmLight(im,dark):
    [h,w] = im.shape[:2]
    imsz = h*w
    numpx = int(max(math.floor(imsz/1000),1))
    darkvec = dark.reshape(imsz);
    imvec = im.reshape(imsz,3);

    indices = darkvec.argsort();
    indices = indices[imsz-numpx::]

    atmsum = np.zeros([1,3])
    for ind in range(1,numpx):
       atmsum = atmsum + imvec[indices[ind]]

    A = atmsum / numpx;
    return A

def TransmissionEstimate(im,A,sz):
    omega = 0.95;
    im3 = np.empty(im.shape,im.dtype);

    for ind in range(0,3):
        im3[:,:,ind] = im[:,:,ind]/A[0,ind]

    transmission = 1 - omega*DarkChannel(im3,sz);
    return transmission

def Guidedfilter(im,p,r,eps):
    mean_I = cv2.boxFilter(im,cv2.CV_64F,(r,r));
    mean_p = cv2.boxFilter(p, cv2.CV_64F,(r,r));
    mean_Ip = cv2.boxFilter(im*p,cv2.CV_64F,(r,r));
    cov_Ip = mean_Ip - mean_I*mean_p;

    mean_II = cv2.boxFilter(im*im,cv2.CV_64F,(r,r));
    var_I   = mean_II - mean_I*mean_I;

    a = cov_Ip/(var_I + eps);
    b = mean_p - a*mean_I;

    mean_a = cv2.boxFilter(a,cv2.CV_64F,(r,r));
    mean_b = cv2.boxFilter(b,cv2.CV_64F,(r,r));

    q = mean_a*im + mean_b;
    return q;

def TransmissionRefine(im,et):
    gray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY);
    gray = np.float64(gray)/255;
    r = 60;
    eps = 0.0001;
    t = Guidedfilter(gray,et,r,eps);

    return t;

def Recover(im,t,A,tx = 0.1):
    res = np.empty(im.shape,im.dtype);
    t = cv2.max(t,tx);

    for ind in range(0,3):
        res[:,:,ind] = (im[:,:,ind]-A[0,ind])/t + A[0,ind]

    return res

if __name__ == '__main__':
    import sys
    try:
        fn = sys.argv[1]
    except:
        fn = r"C:\Users\win10\Desktop\haze.jpg"

    def nothing(*argv):
        pass

    src = cv2.imread(r"C:\Users\win10\Desktop\haze.jpg");
#     cv2.imshow('111',src)
    I = src.astype('float64')/255;
 
    dark = DarkChannel(I,15);
    A = AtmLight(I,dark);
    te = TransmissionEstimate(I,A,15);
    t = TransmissionRefine(src,te);
    J = Recover(I,t,A,0.1);
    
    
    b,g,r = cv2.split(src)
    src = cv2.merge([r,g,b])
    
#     b,g,r = cv2.split(dark)
#     dark = cv2.merge([r,g,b])
    
#     b,g,r = cv2.split(t)
#     t = cv2.merge([r,g,b])
    
    b,g,r = cv2.split(J)
    J = cv2.merge([r,g,b])
    
#     b,g,r = cv2.split(src)
#     src = cv2.merge([r,g,b])
    

    
    plt.figure(figsize=(10,20))
    plt.subplot(3,2,1)
    
    plt.imshow(src)
    plt.title("yuantu")
    
    plt.subplot(3,2,2)
   
    plt.imshow(dark)
    plt.title("dark")
    
    plt.subplot(3,2,3)

    plt.imshow(t)
    plt.title("t")
    
    plt.subplot(3,2,4)

    plt.imshow(J)
    plt.title("output")
    
#     plt.subplot(3,2,5)

#     plt.imshow(J)
#     plt.title("output")
    
    
    plt.show()
    
    
    
#     cv2.imshow("dark",dark);
#     cv2.imshow("t",t);
#     cv2.imshow('I',src);
#     cv2.imshow('J',J);
#     cv2.imwrite("./image/J.png",J*255);
#     cv2.waitKey();
    
    

```

结果：![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/143747_3f3932c4_8933477.png "image-20211005133659821.png")

#### 摘要与网络结构框图

##### 摘要：

在这片文章中主要介绍一下通过dark channel prior(暗通道优先)来去除单一图像中的水雾。这项技术十分适用于户外的一些风景照，这项技术主要是基于一个关键的测试结果，即无雾图片中的一些像素点总是有RGB中的至少一个通道值很小。使用dark channel prior(暗通道优先)技术不仅可以将带雾图片去雾，还可以生成该图片一张高质量深度图。

##### 网络结构框图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/144715_c1d54e79_8933477.png "image-20211006141200430.png")

#### 主要创新点

我认为本论文这个方法主要创新点有如下几点：

* 使用物理方法，通过公式的化简，将所需要的参数通过不同的方法得出，进而得到实验结果。
* 使用单一图片，不需要其他额外的颜色信息，更不需要拍摄多角度相同图像信息的图片等。
* 在得出去雾图后，也可以获取到该图像的深度图，这是由于公式(2)的原因得到的。

#### 步骤与过程

主要公式：![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/145304_9bdd6a0d_8933477.png "image-20211005135323085 - 副本.png")

其中I(x)为input，也就是带雾图像，J(x)为output，也就是输出图像，A是全球大气光成分， t(x)为透射率（透射率为透明体透过光的程度，通常用透过后的[光通量](https://baike.baidu.com/item/光通量/1422628)与入射光通量 之比）。现在的已知条件就是I(x)，要求目标值J(x),显然，这是个有无数解的方程。

需要求出对应的t(x),A，然后才能求出目标值J(x)。

##### Dark Channel Prior理论：

In most of the nonsky patches,at least one color channel has some pixels whose intensity are very low and close to zero。

在一些非天空的带雾/带雨的图像中，一些像素总是至少有一个GRB通道的值极小，光强很小，接近于0。

之所以会提出这个理论，原因主要有三个：

* 阴影，例如汽车，高层建筑和城市中玻璃窗户的阴影。

* 鲜艳的物体的表面，例如，绿色的草地／树／植物，红色或黄色的花朵／叶子，或者蓝色的水面。

* 颜色较暗的物体或者表面，例如灰暗色的树干和石头。

给出暗通道的公式：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/145443_83e716f5_8933477.png "image-20211005141459570 - 副本 - 副本.png")

解释起来就是将每个像素中RGB分量最小的值拿出来，组成一个新的图像。

若J是去雾图像，在非天空区域，J的暗通道是非常低的并且趋近于0。

即：![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/145504_1cf35377_8933477.png "image-20211005142018384 - 副本 (2).png")

##### 估算 the Transmission(t(x))

先假设A给出了一个确定的值，在下边**估算the Atmospheric Light (A)**中给出如何确定一张图片的A。

将公式(1)等式两侧同时除以A，可得：![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/145809_259af6df_8933477.png "image-20211005144727015.png")

然后再去两次最小值，取两次最小值的目的是为了与公式(5),(6)挂钩。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/145823_56ca2668_8933477.png "image-20211005144907144.png")

由于公式(5)(6)，且A不为0，即：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/145845_6b84bb3a_8933477.png "image-20211005145220555.png")![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/145855_ced64ea4_8933477.png "image-20211005145227545.png")

得公式(8)的等号右侧第一项为0。因此化简公式(8)可得：![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150050_01483396_8933477.png "image-20211005145255465.png")

通过公式(11)可得出t(x)。

这个模型受限于对于天空区域并不是最优，不过天空总是与A十分相似，因此在天空区域，我们可以假定：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150109_e193b79a_8933477.png "image-20211005150031496.png")
因此在天空区域![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150422_f10f5d41_8933477.png "image-20211005150057925.png")

即使是晴天白云，空气中也存在着一些颗粒，因此，看远处的物体还是能感觉到雾的影响，另外，雾的存在让人类感到景深的存在，因此，有必要在去雾的时候保留一定程度的雾，这可以通过在公式(11)中引入一个在[0,1] 之间的因子，![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150443_fed71815_8933477.png "image-20211005150513915 - 副本.png")，将公式(11)改写为：![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150619_0cf486e3_8933477.png "image-20211005150609735 - 副本.png")在该论文中，将这个因子 ω设置为0.95.

##### soft matting  

暗通道优先在恢复鲜艳颜色的物体和对比度较低的物体十分有效，但是还是会出现一些halo和block artifacts (光晕和块效应)，这是因为t(x)在一个块中并不总是常量，因此提出使用soft matting 来改善传输系数图。haze imaging model公式和matting model公式非常类似，而image  matting一种常用的做法是，输入图片和trimap图片(包含三部分区域: 白色前景、黑色背景和灰色未知)，用封闭式抠图算法得到alpha  matting。仿照image  matting的方式，图像去雾也可以通过输入图片和transmission图片(类似trimap图片)，得到refined  transmission(类似alpha matting)。

image matting通常用马尔科夫随机场(MRF)来得到alpha matting，于是haze removal也可以通过MRF公式来得到优化后的transmission，公式如下：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1010/092909_14ceb9db_8933477.png "image-20211010092441151.png")

通过拉格朗日乘子法可以得到linear system：![输入图片说明](https://images.gitee.com/uploads/images/2021/1010/092925_146baddd_8933477.png "image-20211010092512949.png")

linear system一般可以通过**Conjugate Gradients**方法计算得到。何博士后续提出了**Large-Kernel**和**Guided Filter**两种方法对linear system计算进行加速。

##### 估算the Atmospheric Light (A)

通过暗通道从有雾图像中获取A，具体步骤如下：

* 从暗通道图中按照亮度的大小取前0.1%的像素。这些像素往往是雾浓度最高的点。一般都是选择图片中最远景处。
*  在这些位置中，在原始有雾图像I中寻找对应的具有最高亮度的点的值，作为A值。注意它们并不一定是整幅图像里最亮的点。

PS:在文章中，起初是假设一个A值，并不是将A根据图像来获取。

这个方法就算是在一个不存在远景的图片中同样适用。

##### Recovering the Scene Radiance(恢复图像) 

由公式(1)变形得到：![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150727_bd1de655_8933477.png "image-20211005153605363.png")



其中多出来的一点是出现了一个t0， 当投射图t 的值趋向于0时，会导致J(x)t(x)的值也趋向于0，从而使得恢复的图片产生噪声，因此一般可设置一最小值t0，当t值小于t0时，令t=t0，本文中所有效果图均以t0=0.1为标准计算。

在4.4中，最后一句话中写到 Since the scene radiance is usually not as bright as the atmospheric light, the image after haze removal looks  dim. So we increase the exposure of J(x) for display.

大致意思为由于scene通常没有the atmospheric light更亮，因此直接去雾，会使图像变得比较暗淡，，因此可以通过增加J(x)的曝光度来更好的显示。

##### patch size（块大小）

patch size在公式11中是一个非常重要的参数之一，在公式11中体现为Ω(x),简称为窗口，其窗口越大，其包含暗通道的概率越大，the dark channel prior (暗通道图)表现就越好(根据公式5得来)。另外若窗口越大，光晕附近的深度边界会显得特别明显，例如论文中fig10，
![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150754_4f120bc2_8933477.png "image-20211006103114498.png")

c中近景的植物和远景之间交叉的地方。

当图片大小等参数固定时，不同窗口大小下恢复图片的效果，在论文Fig11中展现了出来，

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150814_92bfaa31_8933477.png "image-20211006103131053.png")

当选择窗口大小为3*3时，有一些地区的效果显示过饱和。

这个结果显示，当窗口大小取值越大时，这个方法效果越好，配合soft matting，实现去雾的最佳效果。在该论文中，窗口大小选择15*15

#### 结果分析

该实验的结果显示在Fig12中，![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150926_57e5d4c4_8933477.png "image-20211006103014823.png")

其中还展示出了其深度图，深度图是通过![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/150953_720d3614_8933477.png "image-20211006102256538.png")得出，其中t(x)为传输系数，β为散射系数，d(x)为图像深度。该技术可以使用在gray-scale图像中，但是需要忽略minc,Fig13为实验结果。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/151009_778e22c7_8933477.png "image-20211006102957184.png")

然后将本论文的方法与Tan的方法进行比较，结果展示在Fig14![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/151044_f4f06a0d_8933477.png "image-20211006103321690.png")
很显然，本论文方法效果优于Tan的方法，原因是因为Tan的方法，通过最大化对比度进行，往往最大化对比度会高估雾层的浓度。

接下来又与Fattal的方法进行对比，结果展示在Fig15![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/151104_9c186be0_8933477.png "image-20211006103705231.png")

显然效果优于Fattal的方法，他的方法是基于本地数据统计，并且需要足够多的颜色信息和偏差。(b)(c)展示了在实验结果前后使用MRF外推法所导致实验效果的不同。

接下来，又将本论文方法与Kopf et al 的方法所产生的实验结果进行对比，结果展示在Fig16.

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/151131_fad76fa7_8933477.png "image-20211006104509273.png")

Kopf et al需要谷歌地图和卫星地图上的一些额外的信息，而本论文方法只需要一张图片。

最后，又将本论文的方法与其他一些方法进行对比，结果展示在Fig17

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/151151_9779d085_8933477.png "image-20211006105058679.png")

其他方法可能都无法提供一个深度图。

#### 启示

在本论文中提供了一个简单且高效的去雾方法，并且只需要单一图像，这完全是根据一个特性：带雾图片中的一些像素点总是有RGB中的至少一个通道值很小。当然这个方法也有失效的时候，就是当场景物体与大气光想接近，并且没有阴影时，例如Fig18中白色的大理石

![输入图片说明](https://images.gitee.com/uploads/images/2021/1006/151206_2b5862b8_8933477.png "image-20211006105839969.png")

由于本论文这个方法是基于公式(1)，也就是带雾图像模型的，当这个模型无效时，该方法也就失效了。

在上网查看完相关资料，发现去雾算法目前也有着众多其他的方式，不过很多都是以这个为基础，因此，这个学会这个去雾算法能奠定坚实的基础，有助于进一步学习一些当今比较流行的去雾算法。 
