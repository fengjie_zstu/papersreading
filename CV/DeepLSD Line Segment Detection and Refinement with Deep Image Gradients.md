#线识别

Line segments are ubiquitous in our human-made world and are increasingly used in vision tasks. They are complementary to feature points thanks to their spatial extent and the structural information they provide. Traditional line detectors based on the image gradient are extremely fast and accurate, but lack robustness in noisy images and challenging conditions. Their learned counterparts are more repeatable and can handle challenging images, but at the cost of a lower accuracy and a bias towards wireframe lines. We propose to combine traditional and learned approaches to get the best of both worlds: an accurate and robust line detector that can be trained in the wild without ground truth lines. Our new line segment detector, DeepLSD, processes images with a deep network to generate a line attraction field, before converting it to a surrogate image gradient magnitude and angle, which is then fed to any existing handcrafted line detector. Additionally, we propose a new optimization tool to refine line segments based on the attraction field and vanishing points. This refinement improves the accuracy of current deep detectors by a large margin. We demonstrate the performance of our method on low-level line detection metrics, as well as on several downstream tasks using multiple challenging datasets. The source code and models are available at [this https URL](https://github.com/cvg/DeepLSD).  

线段在我们的人造世界中无处不在，并且越来越多地用于视觉任务。由于它们的空间范围和它们提供的结构信息，它们是特征点的补充。传统的基于图像梯度的线检测器非常快速和准确，但在噪声图像和具有挑战性的条件下缺乏鲁棒性。它们的学习对应物更具可重复性，可以处理具有挑战性的图像，但代价是准确性较低，并且偏向于线框线。我们建议将传统方法和学习方法结合起来，以两全其美：一个准确和鲁棒的线检测器，可以在没有地面实况线的情况下在野外训练。我们的新线段检测器DeepLSD使用深度网络处理图像以生成线吸引场，然后将其转换为代理图像梯度幅度和角度，然后将其馈送到任何现有的手工线检测器。 此外，我们提出了一个新的优化工具，以改善线段的吸引力场和消失点的基础上。这种改进大大提高了当前深度检测器的准确性。我们展示了我们的方法在低级别线检测指标上的性能，以及使用多个具有挑战性的数据集在几个下游任务上的性能。源代码和模型可在此https URL获得。

https://arxiv.org/abs/2212.07766v2

https://github.com/cvg/DeepLSD

![[Pasted image 20230321173250.png]]

![[Pasted image 20230321173325.png]]

![[Pasted image 20230321173344.png]]
![[Pasted image 20230321173405.png]]

![[Pasted image 20230321173428.png]]

![[Pasted image 20230321173445.png]]

![[Pasted image 20230321173531.png]]

![[Pasted image 20230321173700.png]]


