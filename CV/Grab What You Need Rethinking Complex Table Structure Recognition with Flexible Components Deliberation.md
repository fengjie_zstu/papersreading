#表格识别 

Recently, Table Structure Recognition (TSR) task, aiming at identifying table structure into machine readable formats, has received increasing interest in the community. While impressive success, most single table component-based methods can not perform well on unregularized table cases distracted by not only complicated inner structure but also exterior capture distortion. In this paper, we raise it as Complex TSR problem, where the performance degeneration of existing methods is attributable to their inefficient component usage and redundant post-processing. To mitigate it, we shift our perspective from table component extraction towards the efficient multiple components leverage, which awaits further exploration in the field. Specifically, we propose a seminal method, termed GrabTab, equipped with newly proposed Component Deliberator. Thanks to its progressive (渐进) deliberation mechanism, our GrabTab can flexibly accommodate to most complex tables with reasonable components selected but without complicated post-processing involved. Quantitative experimental results on public benchmarks demonstrate that our method significantly outperforms the state-of-the-arts, especially under more challenging scenes.  
近年来，表格结构识别（TSR）任务，旨在将表格结构识别为机器可读的格式，在社区中引起了越来越大的兴趣。大多数基于单表组件的方法虽然取得了令人瞩目的成功，但对于内部结构复杂且外部捕获失真的非规则表情况，其性能并不理想。本文将其定义为复杂TSR问题，现有算法的性能退化归因于其低效的组件使用和冗余的后处理。为了缓解这一问题，我们将视角从表组件提取转向高效的多组件利用，这有待于在该领域进一步探索。具体来说，我们提出了一种名为GrabTab的开创性方法，该方法配备了新提出的组件审议器。 得益于其渐进式审议机制，我们的GrabTab可以灵活地适应最复杂的表格，并选择合理的组件，而无需复杂的后处理。在公共基准测试上的定量实验结果表明，该方法的性能明显优于现有技术，尤其是在更具挑战性的场景下。

https://arxiv.org/abs/2303.09174v1

![[Pasted image 20230321174923.png]]

![[Pasted image 20230321174948.png]]

![[Pasted image 20230321175011.png]]

![[Pasted image 20230321175029.png]]

![[Pasted image 20230321175048.png]]

![[Pasted image 20230321175105.png]]

![[Pasted image 20230321175123.png]]
![[Pasted image 20230321175147.png]]
![[Pasted image 20230321175224.png]]
![[Pasted image 20230321175248.png]]
