#表格识别 

Most of the previous methods for table recognition rely on training datasets containing many richly annotated table images. Detailed table image annotation, e.g., cell or text bounding box annotation, however, is costly and often subjective. In this paper, we propose a weakly supervised model named WSTabNet for table recognition that relies only on HTML (or LaTeX) code-level annotations of table images. The proposed model consists of three main parts: an encoder for feature extraction, a structure decoder for generating table structure, and a cell decoder for predicting the content of each cell in the table. Our system is trained end-to-end by stochastic gradient descent algorithms, requiring only table images and their ground-truth HTML (or LaTeX) representations. To facilitate table recognition with deep learning, we create and release WikiTableSet, the largest publicly available image-based table recognition dataset built from Wikipedia. WikiTableSet contains nearly 4 million English table images, 590K Japanese table images, and 640k French table images with corresponding HTML representation and cell bounding boxes. The extensive experiments on WikiTableSet and two large-scale datasets: FinTabNet and PubTabNet demonstrate that the proposed weakly supervised model achieves better, or similar accuracies compared to the state-of-the-art models on all benchmark datasets.  
大多数先前的表格识别方法依赖于包含许多丰富注释的表格图像的训练数据集。详细的表格图像注释，例如，然而，单元格或文本边界框注释是昂贵的并且常常是主观的。本文提出了一种弱监督的表格识别模型WSTabNet，它只依赖于表格图像的HTML（或LaTeX）代码级注释。拟议的模式包括三个主要部分：用于特征提取的编码器、用于生成表结构的结构解码器和用于预测表中每个单元的内容的单元解码器。我们的系统通过随机梯度下降算法进行端到端训练，只需要表格图像及其地面实况HTML（或LaTeX）表示。为了通过深度学习促进表格识别，我们创建并发布了WikiTableSet，这是从维基百科构建的最大的公开可用的基于图像的表格识别数据集。 WikiTableSet包含近400万个英语表格图像、59万个日文表格图像和64万个法文表格图像，以及相应的HTML表示和单元格边框。在WikiTableSet和两个大规模数据集上进行的大量实验：FinTabNet和PubTabNet实验表明，与现有模型相比，本文提出的弱监督模型在所有基准数据集上都能达到更好或相似的精度。

![[Pasted image 20230321180243.png]]

![[Pasted image 20230321180310.png]]

![[Pasted image 20230321180341.png]]

![[Pasted image 20230321180402.png]]
