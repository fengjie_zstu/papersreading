#表格识别 

https://arxiv.org/abs/2303.06949v1

Table structure recognition aims to extract the logical and physical structure of unstructured table images into a machine-readable format. The latest end-to-end image-to-text approaches simultaneously predict the two structures by two decoders, where the prediction of the physical structure (the bounding boxes of the cells) is based on the representation of the logical structure. However, the previous methods struggle with imprecise bounding boxes as the logical representation lacks local visual information. To address this issue, we propose an end-to-end sequential modeling framework for table structure recognition called VAST. It contains a novel coordinate sequence decoder triggered by the representation of the non-empty cell from the logical structure decoder. In the coordinate sequence decoder, we model the bounding box coordinates as a language sequence, where the left, top, right and bottom coordinates are decoded sequentially to leverage the inter-coordinate dependency. Furthermore, we propose an auxiliary visual-alignment loss to enforce the logical representation of the non-empty cells to contain more local visual details, which helps produce better cell bounding boxes. Extensive experiments demonstrate that our proposed method can achieve state-of-the-art results in both logical and physical structure recognition. The ablation study also validates that the proposed coordinate sequence decoder and the visual-alignment loss are the keys to the success of our method.  
表格结构识别的目的是将非结构化表格图像的逻辑和物理结构提取为机器可读的格式。最新的端到端图像到文本方法通过两个解码器同时预测这两种结构，其中物理结构（单元的边界框）的预测基于逻辑结构的表示。然而，由于逻辑表示缺乏局部视觉信息，先前的方法难以处理不精确的边界框。为了解决这个问题，我们提出了一个用于表结构识别的端到端顺序建模框架VAST。它包含一个新颖的坐标序列解码器，该解码器由来自逻辑结构解码器的非空单元的表示触发。在坐标序列解码器中，我们将边界框坐标建模为语言序列，其中左、上、右和下坐标被顺序地解码以利用坐标间依赖性。 此外，我们提出了一个辅助视觉对齐损失来强制非空单元格的逻辑表示包含更多的局部视觉细节，这有助于产生更好的单元格边界框。实验结果表明，该方法在逻辑结构和物理结构识别方面都取得了较好的效果.烧蚀研究也验证了所提出的坐标序列解码器和视觉对准损失是我们方法成功的关键。

![[Pasted image 20230321180520.png]]

![[Pasted image 20230321180552.png]]

![[Pasted image 20230321180608.png]]

![[Pasted image 20230321180640.png]]

![[Pasted image 20230321180701.png]]

![[Pasted image 20230321180715.png]]
![[Pasted image 20230321180736.png]]
![[Pasted image 20230321180750.png]]

![[Pasted image 20230321180808.png]]

![[Pasted image 20230321180833.png]]
