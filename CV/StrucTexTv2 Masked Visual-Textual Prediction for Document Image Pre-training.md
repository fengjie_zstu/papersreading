#文档识别 

https://arxiv.org/abs/2303.00289v1

In this paper, we present StrucTexTv2, an effective document image pre-training framework, by performing masked visual-textual prediction. It consists of two self-supervised pre-training tasks: masked image modeling and masked language modeling, based on text region-level image masking. The proposed method randomly masks some image regions according to the bounding box coordinates of text words. The objectives of our pre-training tasks are reconstructing the pixels of masked image regions and the corresponding masked tokens simultaneously. Hence the pre-trained encoder can capture more textual semantics in comparison to the masked image modeling that usually predicts the masked image patches. Compared to the masked multi-modal modeling methods for document image understanding that rely on both the image and text modalities, StrucTexTv2 models image-only input and potentially deals with more application scenarios free from OCR pre-processing. Extensive experiments on mainstream benchmarks of document image understanding demonstrate the effectiveness of StrucTexTv2. It achieves competitive or even new state-of-the-art performance in various downstream tasks such as image classification, layout analysis, table structure recognition, document OCR, and information extraction under the end-to-end scenario.  
本文提出了一种有效的文档图像预训练框架StructexTv 2，通过对文档图像进行掩蔽视觉-文本预测。它包括两个自我监督的预培训任务：基于文本区域级图像掩蔽的掩蔽图像建模和掩蔽语言建模。该方法根据文本词的包围盒坐标对图像区域进行随机屏蔽。我们的预训练任务的目标是同时重建被掩蔽的图像区域的像素和对应的被掩蔽的表征。因此，与通常预测掩蔽图像块的掩蔽图像建模相比，预训练编码器可以捕获更多的文本语义。与依赖于图像和文本模态的用于文档图像理解的掩蔽多模态建模方法相比，StructexTv 2仅对图像输入进行建模，并可能处理更多不需要OCR预处理的应用场景。 在主流文档图像理解基准上的大量实验验证了StructTexTv2的有效性。它在端到端场景下的图像分类、布局分析、表格结构识别、文档OCR和信息提取等各种下游任务中实现了具有竞争力甚至是全新的最先进性能。

![[Pasted image 20230321181512.png]]
![[Pasted image 20230321181532.png]]

![[Pasted image 20230321181550.png]]

![[Pasted image 20230321181606.png]]
