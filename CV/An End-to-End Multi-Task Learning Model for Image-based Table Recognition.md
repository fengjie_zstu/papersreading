#表格识别 

Image-based table recognition is a challenging task due to the diversity of table styles and the complexity of table structures. Most of the previous methods focus on a non-end-to-end approach which divides the problem into two separate sub-problems: table structure recognition; and cell-content recognition and then attempts to solve each sub-problem independently using two separate systems. In this paper, we propose an end-to-end multi-task learning model for image-based table recognition. The proposed model consists of one shared encoder, one shared decoder, and three separate decoders which are used for learning three sub-tasks of table recognition: table structure recognition, cell detection, and cell-content recognition. The whole system can be easily trained and inferred in an end-to-end approach. In the experiments, we evaluate the performance of the proposed model on two large-scale datasets: FinTabNet and PubTabNet. The experiment results show that the proposed model outperforms the state-of-the-art methods in all benchmark datasets.  
由于表格样式的多样性和表格结构的复杂性，基于图像的表格识别是一项具有挑战性的任务。大多数先前的方法集中于非端到端方法，其将问题划分为两个单独的子问题：表格结构识别;和细胞内容识别，然后尝试使用两个单独的系统独立地解决每个子问题。本文提出了一种基于图像的表格识别的端到端多任务学习模型。该模型由一个共享编码器、一个共享解码器和三个独立解码器组成，用于学习表格识别的三个子任务：表格结构识别、单元格检测和单元格内容识别。整个系统可以通过端到端的方法轻松地进行训练和推断。在实验中，我们在两个大规模数据集上评估了所提模型的性能：金融标签网和公共标签网。 实验结果表明，该模型在所有基准数据集上的性能均优于现有方法。

https://arxiv.org/abs/2303.08648v1

![[Pasted image 20230321175916.png]]

![[Pasted image 20230321175937.png]]

![[Pasted image 20230321180016.png]]
![[Pasted image 20230321180044.png]]
![[Pasted image 20230321180106.png]]

![[Pasted image 20230321180122.png]]
