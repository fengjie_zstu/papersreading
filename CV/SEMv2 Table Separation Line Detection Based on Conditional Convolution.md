#表格识别 

https://arxiv.org/abs/2303.04384v1

Table structure recognition is an indispensable element for enabling machines to comprehend tables. Its primary purpose is to identify the internal structure of a table. Nevertheless, due to the complexity and diversity of their structure and style, it is highly challenging to parse the tabular data into a structured format that machines can comprehend. In this work, we adhere to the principle of the split-and-merge based methods and propose an accurate table structure recognizer, termed SEMv2 (SEM: Split, Embed and Merge). Unlike the previous works in the ``split'' stage, we aim to address the table separation line instance-level discrimination problem and introduce a table separation line detection strategy based on conditional convolution. Specifically, we design the ``split'' in a top-down manner that detects the table separation line instance first and then dynamically predicts the table separation line mask for each instance. The final table separation line shape can be accurately obtained by processing the table separation line mask in a row-wise/column-wise manner. To comprehensively evaluate the SEMv2, we also present a more challenging dataset for table structure recognition, dubbed iFLYTAB, which encompasses multiple style tables in various scenarios such as photos, scanned documents, etc. Extensive experiments on publicly available datasets (e.g. SciTSR, PubTabNet and iFLYTAB) demonstrate the efficacy of our proposed approach. The code and iFLYTAB dataset will be made publicly available upon acceptance of this paper.  
表格结构识别是使机器能够理解表格的不可缺少的元素。它的主要用途是标识表的内部结构。然而，由于表格数据的结构和样式的复杂性和多样性，将表格数据解析为机器可以理解的结构化格式是非常具有挑战性的。在本文中，我们坚持基于拆分和合并的方法的原则，并提出了一种精确的表结构识别器，称为SEMv2（SEM：拆分、嵌入和合并）。与以往工作中的“拆分”阶段不同，本文针对表格分隔线实例级判别问题，提出了一种基于条件卷积的表格分隔线检测策略.具体地说，我们以自顶向下的方式设计"拆分“，首先检测表格分隔线实例，然后动态预测每个实例的表格分隔线掩码。 通过以行/列方式处理表分离线掩模，可以精确地获得最终的表分离线形状。为了全面评估SEMv 2，我们还提出了一个更具挑战性的表格结构识别数据集，称为iFLYTAB，它包含各种场景下的多个样式表，如照片、扫描文档等。在公开数据集（如SciTSR、PubTabNet和iFLYTAB）上的大量实验证明了我们提出的方法的有效性。该代码和iFLYTAB数据集将在接受本文后公开提供。

![[Pasted image 20230321174006.png]]

![[Pasted image 20230321174041.png]]

![[Pasted image 20230321174102.png]]

![[Pasted image 20230321174116.png]]

![[Pasted image 20230321174156.png]]

![[Pasted image 20230321174212.png]]

![[Pasted image 20230321174231.png]]