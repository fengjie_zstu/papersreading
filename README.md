# papersreading

技术文献与代码阅读

按照下面的模版编辑notes

```

# 论文名称

#标签

pdf链接

源码链接

解读

## 摘要与网络结构框图

## 主要创新点

## 步骤和过程

## 结果分析

## 启示

```

李沐：如何读论文
https://www.bilibili.com/video/BV1H44y1t75x

论文阅读计划：

21级新生

- Attention: https://arxiv.org/abs/1706.03762
  - Swin transformer: https://arxiv.org/abs/2103.14030v2
  
- GAN: https://arxiv.org/abs/1406.2661
  - StyleGAN: https://arxiv.org/abs/1812.04948

- OpenPose: https://arxiv.org/abs/1812.08008
  - https://github.com/cagbal/Skeleton-Based-Action-Recognition-Papers-and-Notes
  
- FasterRCNN: https://arxiv.org/abs/1506.01497
  - Maskrcnn: https://arxiv.org/abs/1703.06870

- YOLO: https://arxiv.org/abs/1506.02640 （系列V1-V4）

- GoogleNet: https://arxiv.org/abs/1409.4842
  - mobilenet: https://arxiv.org/abs/1704.04861 （系列v1 v2 v3）

---

邵

图像描述相关论文

---

其他：

unet: https://arxiv.org/abs/1505.04597

https://tkipf.github.io/graph-convolutional-networks/

NAS

---

想法：

1. 持续学习
2. 可解释学习
3. 视觉完形
4. 传统方法与深度学习的结合